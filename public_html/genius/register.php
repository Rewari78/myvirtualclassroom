<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(1);

*/
date_default_timezone_set('Asia/Kolkata');
include "dbConnection.php";


$_POST = json_decode(file_get_contents('php://input'), true);
$current_Date = date("d-m-Y H:i:s");

//POST PARAMS
$FirstName = $_POST['FirstName'];
$LastName = $_POST['LastName'];
$Mobile = $_POST['Mobile'];
$Email = $_POST['Email'];
$Stu_Class = $_POST['Stu_Class'];
$Stu_Board = $_POST['Stu_Board'];
$City = $_POST['City'];
$State = $_POST['State'];
$Country = $_POST['Country'];
$SchoolName = $_POST['SchoolName'];


$sqlEmailCheck = "SELECT * from `EnrollmentMaster` where (`Mob1` = '{$Mobile}' OR `Mob2` = '{$Mobile}' OR `EmailID` = '{$Email}')";
$resEmailCheck = mysqli_query($conn,$sqlEmailCheck);	
$noRowsEmailCheck = mysqli_num_rows($resEmailCheck);
if($noRowsEmailCheck > 0){

  $data['Message'] = 'Email or Mobile number already registered!';
  $data['Status'] = "0";
 

}else{
  
  $sqlInsert = "INSERT INTO `EnrollmentMaster` (`Stu_FirstName`,`Stu_LastName`,`Mob1`,`EmailID`,`Stu_Class`,`Stu_Board`,`Stu_SchoolName`,`City`,`State`,`Country`,`Status`,`CreatedDateTime`) VALUES('{$FirstName}','{$LastName}','{$Mobile}','{$Email}','{$Stu_Class}','{$Stu_Board}','{$SchoolName}','{$City}','{$State}','{$Country}','1','{$current_Date}')";
  
  
  mysqli_query($conn,$sqlInsert);	
   
  
  $data['Message'] = 'User registered successfully';
  $data['Status'] = "1";
  

}



 $sqlData = "SELECT * from `EnrollmentMaster` where `EmailID` = '{$Email}'";
  $resData = mysqli_query($conn,$sqlData);
  while($record = mysqli_fetch_array($resData)){	
    
    	$data['UserDetails']=array(
      	"EnrollmentID"=>$record['EnrollmentID'],
        "FirstName"=>$record['Stu_FirstName'],
       	"LastName"=>$record['Stu_LastName'],
        "Mobile1"=>$record['Mob1'],
        "Mobile2"=>$record['Mob2'],
        "Email"=>$record['EmailID'],
        "Class"=>$record['Stu_Class'],
        "Board"=>$record['Stu_Board'],
        "SchoolName"=>$record['Stu_SchoolName'],
        "City"=>$record['City'],
        "State"=>$record['State'],
        "Country"=>$record['Country'],
        "Status"=>$record['Status']
      
      );
  }
exit(json_encode($data));

?>