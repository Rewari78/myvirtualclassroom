<?php $dates =  array("Monday", "Tuesday", "Wednesday", "Friday", "Saturday", "Sunday");
$times = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id))->result_array();
?><html>
    <head></head>
    <style>
        * {
            font-family: sans-serif;
        }
        @page { margin: 5mm; }        
        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table tr td {
            text-align: center;
            border-top: 2px solid #000;
            border-left: 2px solid #000;
            height: 100px;
        }

        .table tr td:first-child {
            width: 90px;
        }

        .table tr:first-child td {
            height: 50px;
        }

        .table tr td:last-child {
            border-right: 2px solid #000;
        }

        .table tr:last-child td {
            border-bottom: 2px solid #000;
        }

        .break {
            width: 100px;
            background-color: grey;
            color: white;
        }

        .time {
            font-size: 14px;
        }
        h5 {
            text-align: center;
        }
    </style>
<body>
    <div class="list-table">
        <h5>STD- _______________               From Date - ______________      To Date - ________________</h5>
        <table class="table">
            <tr class="table-row">
                <td></td>
                <?php $i = 1; ?>
                <?php foreach( $times as $time ): ?>
                    <td class="<?php echo ($time['type'] == "1" ? "break" : ""); ?>">
                        <?php
                            
                            if ( $time['type'] != "1" )
                            {
                                echo "Period-" . $i;
                                $i++;
                            } else {
                                echo ($time['type'] == "1" ? "Break" : "");
                            }

                            $slot = prepareTimeSlot($time['start_time_hour'], $time['start_time_min']) .'/<br />'. prepareTimeSlot($time['end_time_hour'], $time['end_time_min']);                            
                            echo "<br /><span class='time'>". $slot . "</span>";
                            
                        ?>
                    </td>
                <?php  endforeach; ?>
            </tr>
            <?php foreach ( $dates as $date ): ?>
                <tr>
                    <td><?php echo $date; ?></td>
                    <?php foreach( $times as $time ): ?>
                    <td class="<?php echo $time['type'] == "1" ? "break" : ""; ?>">
                    </td>
                <?php  endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>
</html>
<?php
 function displayDates($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }

 function prepareTimeSlot( $startH, $startM ) 
 {
    $stHour = $startH;
    $stMin = $startM;
    if($stMin<10){
        $stMin = '0'.$stMin;
    }
    
    if($stHour >=12){
        if($stHour != 12)
        $stHour = $startH - 12;
        
        if($startM == 0){
            return $stHour.' PM'; 
        }else{
            return $stHour.':'.$stMin.' PM'; 
        }
        
    }
    else{
        if($startM == 0){
            return $startH. ' AM'; 
        }else{
            return $startH.':'.$stMin. ' AM'; 
        }
    
    }
 }
