<?php

$result = $this->db->query("SELECT * FROM `routines` WHERE `id` = ? ", [$routine_id])->result_array();
$routine = $result[0];

$t_files = json_decode($routine['t_files'], true);
$t_files = is_array($t_files) ? $t_files : array();

?>

<div class="">
    <p>Upload the files which you may need while conducting this class (PDF, Word Doc, Excel, PPT are supported).</p>
    <div class="mb-3">
        <?php if ( isset($t_files[0]) ): ?>
            <a href="<?php echo site_url('uploads/teacher_files/' . $t_files[0]['file']) ?>" target="_blank" style="text-decoration: none;">
                <strong><?php echo $t_files[0]['name'] ?></strong>
            </a> <a href="javascript:void(0);" onclick="removeFile(<?php echo $routine_id; ?>, 0, this)" style="float: right;">Remove</a>
        <?php else: ?>
            <input type="file" class="file-upload-section" onchange="onchangeFile(this, <?php echo $routine_id; ?>)">
            <p class="hide">Uploading...</p>
        <?php endif; ?>
    </div>
    <div class="mb-3">
    <?php if ( isset($t_files[1]) ): ?>
        <a href="<?php echo site_url('uploads/teacher_files/' . $t_files[1]['file']) ?>" target="_blank" style="text-decoration: none;">
                <strong><?php echo $t_files[1]['name'] ?></strong>
            </a> <a href="javascript:void(0);" onclick="removeFile(<?php echo $routine_id; ?>, 1, this)" style="float: right;">Remove</a>
        <?php else: ?>
            <input type="file" class="file-upload-section" onchange="onchangeFile(this, <?php echo $routine_id; ?>)">
            <p class="hide">Uploading...</p>
        <?php endif; ?>
    </div>
    <div class="mb-3">
    <?php if ( isset($t_files[2]) ): ?>
        <a href="<?php echo site_url('uploads/teacher_files/' . $t_files[2]['file']) ?>" target="_blank" style="text-decoration: none;">
                <strong><?php echo $t_files[2]['name'] ?></strong>
            </a> <a href="javascript:void(0);" onclick="removeFile(<?php echo $routine_id; ?>, 2, this)" style="float: right;">Remove</a>
        <?php else: ?>
            <input type="file" class="file-upload-section" onchange="onchangeFile(this, <?php echo $routine_id; ?>)">
            <p class="hide">Uploading...</p>
        <?php endif; ?>
    </div>
</div>