<?php
/*error_reporting(E_ALL & ~E_NOTICE);

//phpinfo();
$mc = new Memcached();
$mc->addServer("127.0.0.1", 11211);

$mc->set("foo", "Hello!");
$mc->set("bar", "Memcached...");

$arr = array(
    $mc->get("foo"),
    $mc->get("bar")
);
var_dump($arr);*/
?>

<?php
    $fromDate = date('m/d/Y');
    $toDate = displayDates($fromDate, 14);
    $nextDate = $toDate[1];
    $toDate = $toDate[count($toDate) - 1];       

    if ( !empty($_POST) ) {
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
    }
    
    


?>



<!-- start page title -->
<div class="row ">
<div class="col-xl-12">
    <div class="card">
    <div class="card-body">
        <h4 class="page-title"> <?php echo 'Today\'s Date - ' . date('jS F [l]'); ?> </h4>
    </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
</div>
<!-- end page title -->

<div class="row">
<div class="col-xl-12">
    <div class="card p-3">
        <form action="#" method="post">
        <div class="row">
            
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label ><?php echo 'From Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="fromdate" onchange="onfromdateChange(this.value);" value="<?php echo $fromDate; ?>" name="fromdate"  data-provide = "datepicker" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label><?php echo 'To Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="todate" name="todate" data-provide = "datepicker" data-date-start-date="<?php echo $fromDate;  ?>" required value="<?php echo $toDate; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" type="submit" ><?php echo get_phrase('submit'); ?></button>
            </div>
            
        </div>
        </form>
    </div>
</div>
</div>

<?php 
$timestamp = strtotime($fromDate);
$nexttimestamp = strtotime($toDate);

$teacher = $this->db->query("SELECT * FROM `teachers` WHERE `user_id` = ?", [$this->session->userdata['user_id']])->result_array();
$teacher_id = count($teacher) >= 1 ? $teacher[0]['id'] : null;

if ( !$teacher_id ) {
    echo "You are not a teacher.";exit;
}

$SQL = "SELECT * FROM `routines` WHERE `teacher_id` = ? AND `school_id` = ? AND `rawdate` >= ? AND `rawdate` <= ? ORDER BY `rawdate` ASC";
$routines  = $this->db->query($SQL, [$teacher_id, school_id(), $timestamp, $nexttimestamp])->result_array();

// Set sections
$SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
 
$sections = array();

//GEt teachers
$SQL = "SELECT `t`.`id`, `u`.`id` AS `user_id`, `u`.`name` FROM `users` AS `u`
        INNER JOIN `teachers` AS `t`
        ON ( `u`.`id` = `t`.`user_id` )
        INNER JOIN `routines` AS `r`
        ON ( `r`.`teacher_id` = `t`.`id` )
        ";
$result = $this->db->query($SQL)->result_array();
$teachers = array();
foreach ( $result as $row )
{
    $teachers[$row['id']] = $row;
}

// Set sections
$SQL = "SELECT * FROM `common_subject`";
$result = $this->db->query($SQL)->result_array();
$subjects = array();
foreach( $result as $row )
{
    $subjects[$row['id']] = $row;
}

?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card p-3">
            <?php if ( count($routines) >= 1 ): ?>
            <?php
                $times = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id, 'type' => 0))->result_array();
                $firstTime = $routines[0]['date'];
                $dateList = displayDates2($firstTime, $toDate);
            ?>
            <div class="list-table">
        <table class="table">
        <?php $i = 1; ?>
        <?php foreach( $times as $time ): ?>            
        <?php
            $slot = prepareTimeSlot($time['start_time_hour'], $time['start_time_min']) .'-'. prepareTimeSlot($time['end_time_hour'], $time['end_time_min']);
            $timeIndex[] = $slot;
        ?>
        <?php  endforeach; ?>                    
            <?php foreach ( $dateList as $date ): ?>
                
                <tr>
                    <td><?php                     
                    $day = date('l', strtotime($date));
                    echo $day . "<br />";
                    echo $date; 
                    
                    
                    ?></td>

                        <?php


                        $dbValues = array($date, $teacher_id);
                        $SQL = "SELECT * FROM `routines` 
                            WHERE `date` = ? AND `teacher_id` = ? ";
                        $routines = $this->db->query($SQL, $dbValues)->result_array();

                        if ( count($routines) <= 0 ) {
                            echo '<td class="routine-gap text-center" colspan="' . count($timeIndex) . '">No Scheduled classes</td>';
                            continue;
                        }
                        ?>
                    <?php foreach( $timeIndex as $time ): ?>
                        <?php
                        $dbValues = array($date, $time, $teacher_id);
                        $SQL = "SELECT * FROM `routines` 
                            WHERE `date` = ? AND `htimeslot` = ? AND `teacher_id` = ? ";
                       
                        
                        $routines = $this->db->query($SQL, $dbValues)->result_array();                                
                        ?>
                        <td class="<?php echo empty($routines) ? 'empty' : '' ?>" >
                            <?php                                
                                foreach ( $routines as $routine )
                                {
                                    $teacherName = isset($teachers[$routine['teacher_id']]) ? $teachers[$routine['teacher_id']]['name'] : 'Unknown';
                                    $subjectName = isset($subjects[$routine['subject_id']]) ? $subjects[$routine['subject_id']]['name'] : 'Unknown';

                                    // Section detection
                                    // Set sections                                    
                                    $SQL = "SELECT * FROM `sections` WHERE `class_id` = ? AND `id` IN (" . $routine['section_id'] . ")";
                                    $result = $this->db->query($SQL, [$routine['class_id']])->result_array();
                                    $sections = array();
                                    foreach( $result as $row )
                                    {
                                        $sections[$row['id']] = $row;
                                    }

                                    $avlSections = array();
                                    foreach ( explode(',', $routine['section_id'])  as $id )
                                    {
                                        $avlSections[] = isset($sections[$id]) ? $sections[$id]['name'] : 'NA';
                                    }

                                    $avlSections = implode(',', $avlSections);                                    

                                    // Class declaration
                                    $SQL = "SELECT * FROM `classes` WHERE `id` = ? AND `school_id`  = ?";
                                    $result = $this->db->query($SQL, [$routine['class_id'], school_id()])->result_array();

                                    $className = count($result) >= 1 ? $result[0]['name']: 'NA';
                                    $std = count($result) >= 1 ? $result[0]['name'] . ' - ' : 'NA - ';
                                    $std .= $avlSections;
                                    $comment = ($routine['t_comment'] ? $routine['t_comment'] : '');
                                    $small = mb_strlen($comment)  > 33 ? mb_substr($comment, 0, 33) . '...' : $comment;                                    
                                    $tttt = ( strtotime($routine['date']) + ($routine['starting_hour'] * 60 * 60) + ($routine['starting_minute'] * 60)) * 1000;
                                    $endtt = ( strtotime($routine['date']) + ($routine['ending_hour'] * 60 * 60) + ($routine['ending_minute'] * 60)) * 1000;
                                    $duration = ( $endtt - $tttt ) / 1000 / 60;
                                    $isFinished = ($endtt / 1000) - time() <= 10 ? true : false;
                                    $meetings = $this->db->query("SELECT * FROM `meetings` WHERE `routine_id` = ?", [$routine['id']])->result_array();

                                    $rid = $routine['id'];
                                    $output = "<strong>ID:</strong> $rid <br /><strong>Time:</strong> $time<br /> <strong>Subject:</strong> $subjectName<br /><strong>STD:</strong> $std<br />";
                                    $output .= '<div class="comment-update mt-1" style="max-width: 173px;white-space: normal"> ' .  $small . '</div>';
                                    $output .= '<div class=" hide comment-hidden-' . $routine['id'] . '" style="max-width: 173px;white-space: normal"> ' .  $comment . '</div>';
                                    $output .= '<a href="javascript:void(0)" class="add-edit-c" style="color: white;text-decoration: underline;" onclick="viewComment(' . $routine["id"] . ', this);">' .  ( empty($routine['t_comment']) ?  'Add Comment' : 'Edit Comment' ) . '</a>';                                    

                                    if ( empty($meetings) && !$isFinished ) {
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" id="startClass" name="startClass" onclick="startclass(' . $routine['id'] . ', ' . $tttt . ', ' . $endtt . ', \'' . $subjectName . '\',\'' . $teacherName . '\', ' . $duration . ');">Start Class</button>';
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" id="startClass" name="startClass" onclick="uploadFiles(' . $routine['id'] . ');"><i class="fa fa-upload"></i> Upload Files</button>';
                                    } else if ( !$isFinished ) {
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" onclick="joinMeeting(this, '. $endtt . ', \'' . $meetings[0]['moderatorJoinLink'] . '\')">Rejoin Class</button>';
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" onclick="deleteMeeting(' . $routine['id'] . ')">Reset Meeting</button>';
                                    }

                                    $output .= '<br /><a href="' . route('download_a/' . str_replace('/', '-', $routine['date']) . '/' . $subjectName . '/' . str_replace(',', '-', $routine['section_id']) . '/' . $rid . '/' . $className . '/' .   str_replace(',', '-',$avlSections) . '/' . $time) . '" class="btn btn-light mt-2" target="_blank">Download Attendance</a>';
                                    
                                    
                                    echo '<div class="cal-slot">';
                                    echo $output;
                                    echo '</div>';
                                }
                            ?>
                        </td>
                        
                    <?php endforeach; ?>
                    
                </tr>                
            <?php endforeach; ?>
        </table>
    </div>

            <?php else: ?>
                No schedule found.
            <?php endif; ?>
        </div>
    </div><!-- end col-->
</div>

<script>
$(document).ready(function() {
    initDataTable("expense-datatable");
});

function deleteMeeting( id ) {
    if ( confirm("Do you realy want to delete this meeting?") ) {
      $.ajax({
        url: '<?php echo route('section/deletemeeting/') ?>',
        data: {
            id: id,
        },
        type: 'POST',
        success: function(response) {
            if ( response.success ) {
               window.location.reload(); 
            }
        }
      });
        
    }
}

function startclass( id, timetostart, endTime, subjectName, teacherName) {
    
    // $('#startClass').attr("disabled", true);
    
    $.ajax({
        url: '<?php echo route('section/start/') ?>',
        data: {
            id: id,
            start: timetostart
        },
        type: 'POST',
        success: function(response) {
            if ( response.success ) {

                var comment = $('comment-hidden-' + id).text();
                var dialog = bootbox.dialog({
                    message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Please wait while start the meeting...</p>',
                    closeButton: false
                });
                
                var timeNow = new Date().getTime();
                duration = Math.round((endTime - timeNow) / 1000 / 60);
                
                $.ajax({
                    url: '<?php echo route('section/create/') ?>',
                    data: {
                        id: id,
                        start: timetostart,
                        subject: subjectName,
                        teacher: teacherName,
                        msg: comment,
                        duration: duration
                    },                    
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        if ( response.success ) {
                            window.open(response.join, '_blank');
                            window.location.reload();
                             
                            // window.open(response.join, '_blank');
                            window.location.href = response.join;

                        }

                        dialog.modal('hide');
                    }
                });

            } else {                
                bootbox.alert("You can conduct this Class just before or on alloted timing only");
            }
        }
    });
}


function uploadFiles( id )
{
    var waitModel = bootbox.dialog({
            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Opening file uploader...</p>',
            closeButton: false,
            animate: false,
            onShow: function() {
                // Send the ajax for data
                $.ajax({
                    url: '<?php echo route('section/upload/') ?>',
                    data: {
                        id: id,
                    },
                    type: 'POST',
                    success: function(response) {                                
                        waitModel.modal('hide');
                        bootbox.dialog({
                            message: response,
                            closeButton: true,
                        });
                    }
                });
            }
    });

}

function onchangeFile(elm, id) {

    var file = elm.files[0];
    var ext =  file.name.split('.').pop();
  

    switch( ext.toLowerCase() ) {
        case 'pdf':
        case 'doc':
        case 'docx':
        case 'xls':
        case 'xlsx':
        case 'ppt':
        case 'pptx':
        break;
        case 'mp4':
        case 'mov':
        case 'avi':
            toastr.error("Video file cannot be uploaded here. You can show video during the classes in the form of youtube link ( For Eg - https://www.youtube.com/watch?v=43G7DnxgJCI )");
            $(elm).val('');
        return 1;
        default:
            toastr.error(".pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx files are allowed to upload.");
            $(elm).val('');
        return 1;
    }
            
    var formData = new FormData();
    formData.append('id', id);
    formData.append('file', file);     

    $.ajax({
        url: '<?php echo route('section/upload_file/') ?>',
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        beforeSend: function() {
            $(elm).hide().next('p').removeClass('hide');
        },
        success: function(response) {   
            if ( response.success ) {
                $(elm).hide().next('p').parent('div').html(response.message);
            }
            
        }
    });
}


function removeFile(id, index, elm)
{
    $.ajax({
        url: '<?php echo route('section/remove_file/') ?>',
        data: {
            id: id,
            index: index
        },
        type: 'POST',
        beforeSend: function() {
            
        },
        success: function(response) {   
            if ( response.success ) {
                $(elm).parent('div').html(response.message);
            }
        }
    });
}

function joinMeeting( elm, endTime, url ) {    
    if ( endTime - new Date().getTime() >= 10000 ) {
        window.open(url, '_blank');
    } else {
        $(elm).remove();
    }    
}

function viewComment( id, elm ) {
    bootbox.prompt({
        title: "Add/Edit Comment",
        inputType: 'textarea',
        value: $(elm).prev().text(),
        centerVertical: true,
        closeButton: false,
        callback: function(result){ 
            if ( !result ) {
                $('.add-edit-c').text("Add Comment");
                return;
            }
            if ( result.trim() === "" ) {
                toastr.error('Please add a comment before save.');
                return false;
            }


            toastr.success('Comment added.');

            $(elm).text("Edit Comment");

            $(elm).prev().text(result);
            $(elm).prev().prev().text(result.length > 33 ? result.substr(0, 33) + '...' : result);

            $.ajax({
                url: "<?php echo route('dashboard/savecomment/'); ?>",
                type: 'POST',
                data: {
                    id: id,
                    comment: result
                }
            });            
        }
    });
}

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}


</script>

<?php

function displayDates($date1, $upto, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);  
    $stepVal = '+1 day';
    $i = 0;
    while( $i < $upto ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
       $i++;
    }
    return $dates;
  }


  function displayDates2($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }

function dateGap($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return count($dates);
 }


function prepareTimeSlot( $startH, $startM ) 
{
   $stHour = $startH;
   $stMin = $startM;
   if($stMin<10){
       $stMin = '0'.$stMin;
   }
   
   if($stHour >=12){
       if($stHour != 12)
       $stHour = $startH - 12;
       
       if($startM == 0){
           return $stHour.' PM'; 
       }else{
           return $stHour.':'.$stMin.' PM'; 
       }
       
   }
   else{
       if($startM == 0){
           return $startH. ' AM'; 
       }else{
           return $startH.':'.$stMin. ' AM'; 
       }
   
   }
}


