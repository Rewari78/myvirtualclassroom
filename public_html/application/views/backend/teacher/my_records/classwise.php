<!--title-->
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title">
					<?php echo 'Classwise'; ?>					
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="row mt-3  pl-3 pr-3 top-level">	
				
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label ><?php echo 'Class'; ?></label>
						<select name="class" id="class_id" class="form-control select2" data-toggle="select2" required onchange="classWiseSection(this.value)">
							<option value=""><?php echo get_phrase('select_a_class'); ?></option>
							<?php
							$classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
							foreach($classes as $class){
								?>
								<option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>				

				<div class="col-md-4 mb-1 section-combo">
					<div class="form-group">
						<label><?php echo 'Sections'; ?></label>												
						<div class="section-combo-list">
							Select class first
						</div>
					</div>
				</div>
					<!-- <input type="text" value="" class="form-control" id="date" name = "date" data-provide="datepicker" data-date-start-date="<?php // echo date('m/d/Y', time()); ?>"  required onchange="onchangedate();">
					<select name="section" id="section_id" class="form-control select2" data-toggle="select2" required>
						<option value=""><?php // echo get_phrase('select_section'); ?></option>
					</select> -->				
				<div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" onclick="filter_class_routine()" type="button"><?php echo get_phrase('submit'); ?></button>
				</div>
			</div>
			<div class="card-body class_routine_content">
				<?php include 'list.php'; ?>
			</div>
		</div>
	</div>
</div>

<script>

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}

function classWiseSection(classId) {
	if ( classId )	
	{
		$.ajax({
		url: "<?php echo route('section/sectioncombo/'); ?>",
		data: {
			classId: classId
		},
		type: 'POST',
		success: function(response){
			$('.section-combo-list').html(response);
		}
		});
	} else {
		$('.section-combo-list').html('Select class first');
	}
	
}

function filter_class_routine()
{
	var class_id = $('#class_id').val();	
	var values = [];
    $('.combo-section-input').each(function(i, v) {
        if ( v.checked ) {
            values.push(v.value);
        }
	});
	
	if ( !class_id || values.length <= 0  ) {
		toastr.error('Please select Class and Sections to Filter');
		return;
	};

	getFilteredClassRoutine( 1 );
}

var getFilteredClassRoutine = function( pageNo ) {
	var class_id = $('#class_id').val();
	var from_date = $('#fromdate').val();
	var to_date = $('#todate').val();

    var values = [];
    $('.combo-section-input').each(function(i, v) {
        if ( v.checked ) {
            values.push(v.value);
        }
	});
	
	if ( values.length <= 0  ) return;
	
	if( class_id != "" ) {
		$.ajax({
			url: '<?php echo route('section/filter/') ?>',
			data: {
				classId: class_id,
				sectionIds: JSON.stringify(values),
                pageNo: pageNo
			},
			type: 'POST',
			success: function(response) {
				$('.class_routine_content').html(response);
			}
		});
	}
}
</script>
