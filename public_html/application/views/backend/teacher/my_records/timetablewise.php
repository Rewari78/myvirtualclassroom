<?php
function displayDatesOld($date1, $upto, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);  
    $stepVal = '-1 day';
    $i = 0;
    while( $i < $upto ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
       $i++;
    }
    return $dates;
  }

    $oldDates = displayDatesOld(date('m/d/Y'), 14);    
    $oldDates = array_reverse($oldDates);    
    $toDate = $oldDates[count($oldDates) - 1];
    $fromDate = $oldDates[0];

    if ( !empty($_POST) ) {
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
    }


?>
<!-- start page title -->
<div class="row ">
<div class="col-xl-12">
    <div class="card">
    <div class="card-body">
        <h4 class="page-title"> <?php echo 'Time Tablewise'; ?> </h4>
    </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
</div>
<!-- end page title -->

<div class="row">
<div class="col-xl-12">
    <div class="card p-3">
        <form action="#" method="post">
        <div class="row">
            
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label ><?php echo 'From Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="fromdate" onchange="onfromdateChange(this.value);" value="<?php echo $fromDate; ?>" name="fromdate"  data-provide = "datepicker" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label><?php echo 'To Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="todate" name="todate" data-provide = "datepicker" data-date-start-date="<?php echo $fromDate;  ?>" required value="<?php echo $toDate; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" type="submit" ><?php echo get_phrase('submit'); ?></button>
            </div>
            
        </div>
        </form>
    </div>
</div>
</div>

<?php 
$timestamp = strtotime($fromDate);
$nexttimestamp = strtotime($toDate);

$teacher = $this->db->query("SELECT * FROM `teachers` WHERE `user_id` = ?", [$this->session->userdata['user_id']])->result_array();
$teacher_id = count($teacher) >= 1 ? $teacher[0]['id'] : null;

if ( !$teacher_id ) {
    echo "You are not a teacher.";exit;
}

$SQL = "SELECT * FROM `routines` WHERE `teacher_id` = ? AND `school_id` = ? AND `rawdate` >= ? AND `rawdate` <= ? ORDER BY `rawdate` ASC";
$routines  = $this->db->query($SQL, [$teacher_id, school_id(), $timestamp, $nexttimestamp])->result_array();

// Set sections
$SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
 
$sections = array();

//GET teachers
$SQL = "SELECT `t`.`id`, `u`.`id` AS `user_id`, `u`.`name` FROM `users` AS `u`
        INNER JOIN `teachers` AS `t`
        ON ( `u`.`id` = `t`.`user_id` )
        INNER JOIN `routines` AS `r`
        ON ( `r`.`teacher_id` = `t`.`id` )
        ";
$result = $this->db->query($SQL)->result_array();
$teachers = array();
foreach ( $result as $row )
{
    $teachers[$row['id']] = $row;
}

// Set sections
$SQL = "SELECT * FROM `common_subject`";
$result = $this->db->query($SQL)->result_array();
$subjects = array();
foreach( $result as $row )
{
    $subjects[$row['id']] = $row;
}

?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card p-3">
            <?php if ( count($routines) >= 1 ): ?>
            <?php
                $times = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id, 'type' => 0))->result_array();
                $firstTime = $routines[0]['date'];
                $dateList = displayDates2($firstTime, $toDate);
            ?>
            <div class="list-table">
        <table class="table">
        <?php $i = 1; ?>
        <?php foreach( $times as $time ): ?>            
        <?php
            $slot = prepareTimeSlot($time['start_time_hour'], $time['start_time_min']) .'-'. prepareTimeSlot($time['end_time_hour'], $time['end_time_min']);
            $timeIndex[] = $slot;
        ?>
        <?php  endforeach; ?>      
        <?php $dateList = array_reverse($dateList); ?>                              
            <?php foreach ( $dateList as $date ): ?>
                
                <tr>
                    <td><?php                     
                    $day = date('l', strtotime($date));
                    echo $day . "<br />";
                    echo date('[d - F]', strtotime($date)); 
                    
                    
                    ?></td>

                        <?php


                        $dbValues = array($date, $teacher_id);
                        $SQL = "SELECT * FROM `routines` 
                            WHERE `date` = ? AND `teacher_id` = ? ";

                        $routines = $this->db->query($SQL, $dbValues)->result_array();

                        if ( count($routines) <= 0 ) {
                            echo '<td class="routine-gap text-center" colspan="' . count($timeIndex) . '">No Scheduled classes</td>';
                            continue;
                        }
                        ?>
                    <?php foreach( $timeIndex as $time ): ?>
                        <td>
                            <?php
                                $dbValues = array($date, $time, $teacher_id);
                                $SQL = "SELECT * FROM `routines` 
                                    WHERE `date` = ? AND `htimeslot` = ? AND `teacher_id` = ? ";

                                if ( !empty($section_ids) ) 
                                {
                                    $placeHolders = array();
                                    foreach ( $section_ids as $section )
                                    {
                                        $dbValues[] = $section;
                                        $placeHolders[] = '?';
                                    }

                                    $SQL .= " AND `section_id` IN (" . implode(",", $placeHolders) . ") ";
                                }
                                $routines = $this->db->query($SQL, $dbValues)->result_array();                                
                                foreach ( $routines as $routine )
                                {
                                    $teacherName = isset($teachers[$routine['teacher_id']]) ? $teachers[$routine['teacher_id']]['name'] : 'Unknown';
                                    $subjectName = isset($subjects[$routine['subject_id']]) ? $subjects[$routine['subject_id']]['name'] : 'Unknown';

                                    // Section detection
                                    // Set sections                                    
                                    $SQL = "SELECT * FROM `sections` WHERE `class_id` = ? AND `id` IN (" . $routine['section_id'] . ")";
                                    $result = $this->db->query($SQL, [$routine['class_id']])->result_array();
                                    $sections = array();
                                    foreach( $result as $row )
                                    {
                                        $sections[$row['id']] = $row;
                                    }

                                    $avlSections = array();
                                    foreach ( explode(',', $routine['section_id'])  as $id )
                                    {
                                        $avlSections[] = isset($sections[$id]) ? $sections[$id]['name'] : 'NA';
                                    }

                                    $avlSections = implode(',', $avlSections);                                    

                                    // Class declaration
                                    $SQL = "SELECT * FROM `classes` WHERE `id` = ? AND `school_id`  = ?";
                                    $result = $this->db->query($SQL, [$routine['class_id'], school_id()])->result_array();

                                    $SQL = "SELECT * FROM `meetings` WHERE `routine_id` = ?";
                                    $meeting = $this->db->query($SQL, [$routine['id']])->result_array();

                                    $std = count($result) >= 1 ? $result[0]['name'] . ' - ' : 'NA - ';
                                    $std .= $avlSections;
                                    $comment = ($routine['t_comment'] ? $routine['t_comment'] : '');
                                    $small = mb_strlen($comment)  > 33 ? mb_substr($comment, 0, 33) . '...' : $comment;
                                    $output = "<strong>STD:</strong> $std<br /><strong>Subject:</strong> $subjectName";
                                    $output .= ( !empty($routine['t_comment']) ? '<div class="comment-update" style="max-width: 173px;white-space: normal">' .  (mb_strlen($routine['t_comment']) > 33 ? mb_substr($routine['t_comment'], 0, 33) . '...' : $routine['t_comment']) . '</div>' : '');
                                    $output .= ( !empty($routine['t_comment']) ? '<a href="javascript:void(0)" style="color: white;text-decoration: underline;" onclick="viewComment(' . $routine["id"] . ', this);">View Comment</a>' : '' );
                                    $output .= ( !empty($routine['t_comment']) ? '<div class="hide comment-update">' .  $routine['t_comment'] . '</div>' : '');
                                    

                                    if ( empty($meeting) || empty($meeting[0]['recordingLink']) ) {
                                        $output .= '<br />There is no recording found yet.';
                                    } else {
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" onclick="seerecording(\'' . $meeting[0]['recordingLink'] .'\')"><i class="fa fa-play" aria-hidden="true"></i></button>';
                                    }

                                    echo '<div class="cal-slot">';
                                    echo $output;
                                    echo '</div>';
                                }
                            ?>
                        </td>
                        
                    <?php endforeach; ?>
                    
                </tr>                
            <?php endforeach; ?>
        </table>
    </div>

            <?php else: ?>
                No schedule found.
            <?php endif; ?>
        </div>
    </div><!-- end col-->
</div>

<script>
$(document).ready(function() {
    initDataTable("expense-datatable");
});

function viewComment( id, elm ) {
    var text = $(elm).next().text().trim() === "" ? "No comment yet" : $(elm).next().text().trim();
    bootbox.alert({
        size: "small",
        title: "Comment by Teacher",
        message: text,
        callback: function(){ /* your callback code */ }
    });   
}

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}

function seerecording( url ) {
    window.open(url, '_blank');
}

</script>

<?php

function displayDates($date1, $upto, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);  
    $stepVal = '+1 day';
    $i = 0;
    while( $i < $upto ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
       $i++;
    }
    return $dates;
  }


  function displayDates2($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }

function dateGap($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return count($dates);
 }


function prepareTimeSlot( $startH, $startM ) 
{
   $stHour = $startH;
   $stMin = $startM;
   if($stMin<10){
       $stMin = '0'.$stMin;
   }
   
   if($stHour >=12){
       if($stHour != 12)
       $stHour = $startH - 12;
       
       if($startM == 0){
           return $stHour.' PM'; 
       }else{
           return $stHour.':'.$stMin.' PM'; 
       }
       
   }
   else{
       if($startM == 0){
           return $startH. ' AM'; 
       }else{
           return $startH.':'.$stMin. ' AM'; 
       }
   
   }
}


