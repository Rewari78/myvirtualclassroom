<?php if (isset($class_id) && isset($section_ids) && isset($page_no)) : ?>
    <?php

    //GEt teachers
    $SQL = "SELECT `t`.`id`, `u`.`id` AS `user_id`, `u`.`name` FROM `users` AS `u`
                INNER JOIN `teachers` AS `t`
                ON ( `u`.`id` = `t`.`user_id` )
                INNER JOIN `routines` AS `r`
                ON ( `r`.`teacher_id` = `t`.`id` )
                ";
    $result = $this->db->query($SQL)->result_array();
    $teachers = array();
    foreach ($result as $row) {
        $teachers[$row['id']] = $row;
    }

    $teacher = $this->db->query("SELECT * FROM `teachers` WHERE `user_id` = ?", [$this->session->userdata['user_id']])->result_array();
    $teacher_id = count($teacher) >= 1 ? $teacher[0]['id'] : null;

    // Set sections
    $SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
    $result = $this->db->query($SQL, [$class_id])->result_array();
    $sections = array();
    foreach ($result as $row) {
        $sections[$row['id']] = $row;
    }
    // Set sections
    $SQL = "SELECT * FROM `common_subject`";
    $result = $this->db->query($SQL)->result_array();
    $subjects = array();
    foreach ($result as $row) {
        $subjects[$row['id']] = $row;
    }



    // get total routines.
    $SQL = "
            SELECT count(*) AS `count` FROM `routines` AS `r`            
            WHERE `r`.`teacher_id`  = ?
            AND `r`.`class_id` = ?
            AND `r`.`school_id` = ?
            AND `r`.`id` IN (
                SELECT `routine_id` FROM `meetings` WHERE `isRecorded` = 1
            )
            AND `r`.`section_id` IN
        ";

    $dbValues = array($teacher_id, $class_id, school_id());
    $placeHolders = array_fill(0, count($section_ids), '?');

    foreach ($section_ids as $section) {
        $dbValues[] = $section;
    }

    $SQL .= "(" . implode(',', $placeHolders) . ") ORDER BY `r`.`rawdate` DESC";
    $count = $this->db->query($SQL, $dbValues)->result_array();

    $page_limit = 10;
    $total_pages = ceil($count[0]['count'] / $page_limit);
    $skip = ($page_no - 1) * $page_limit;


    // the routines.
    $SQL = "
            SELECT `r`.* FROM `routines` AS `r`            
            WHERE `r`.`teacher_id`  = ?
            AND `r`.`class_id` = ?
            AND `r`.`school_id` = ?
            AND `r`.`id` IN (
                SELECT `routine_id` FROM `meetings` WHERE `isRecorded` = 1
            )
            AND `r`.`section_id` IN
        ";

    $dbValues = array($teacher_id, $class_id, school_id());
    $placeHolders = array_fill(0, count($section_ids), '?');

    foreach ($section_ids as $section) {
        $dbValues[] = $section;
    }

    $SQL .= "(" . implode(',', $placeHolders) . ") ORDER BY `r`.`rawdate` DESC LIMIT $skip, $page_limit";
    $routines = $this->db->query($SQL, $dbValues)->result_array();
    ?>
    <div class="list-table">
        <table class="table">
            <tr>
                <td>Day/Date</td>
                <td>Held For</td>
                <td>My Comments</td>
                <td>Subject</td>
                <td>Play</td>
            </tr>
            <?php foreach ($routines as $routine) : ?>
                <tr>
                    <td><?php echo date('D/jS F', $routine['rawdate']); ?></td>
                    <td>
                        <?php
                        $avlSections = array();
                        foreach (explode(',', $routine['section_id'])  as $id) {
                            $avlSections[] = isset($sections[$id]) ? $sections[$id]['name'] : 'NA';
                        }
                        $avlSections = implode(',', $avlSections);

                        // Class declaration
                        $SQL = "SELECT * FROM `classes` WHERE `id` = ? AND `school_id`  = ?";
                        $result = $this->db->query($SQL, [$routine['class_id'], school_id()])->result_array();

                        $std = count($result) >= 1 ? $result[0]['name'] . ' - ' : 'NA - ';
                        $std .= $avlSections;

                        echo $std;
                        ?>
                    </td>
                    <td style="max-width: 200px;white-space: normal;"><?php
                                        if ( !empty($routine['t_comment']) ) {
                                        echo (mb_strlen($routine['t_comment'])  > 120 ? mb_substr($routine['t_comment'], 0, 120) . '...' : $routine['t_comment']);
                                        echo '<br /><a href="javascript:void(0)" onclick="viewComment(this)">View Comment</a>';
                                        echo '<div class="hide">' . $routine['t_comment'] . '</div>';
                                        } else {
                                            echo 'No comments about this Class from you yet';
                                        }
                                                                        ?></td>
                    <td><?php echo isset($subjects[$routine['subject_id']]) ? $subjects[$routine['subject_id']]['name'] : 'Unknown'; ?></td>
                    <?php
                    $SQL = "SELECT * FROM `meetings` WHERE `routine_id` = ?";
                    $meeting = $this->db->query($SQL, [$routine['id']])->result_array();
                    ?>
                    <td><button class="btn btn-light" style="box-shadow: none;" onclick="seerecording('<?php echo $meeting[0]['recordingLink'] ?>')"><i class="fa fa-play" aria-hidden="true"></i></button></td>                    
                    </tr> <?php endforeach; ?> 
            </table>
             </div> 
                <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="basic-datatable_info" role="status" aria-live="polite">Showing <?php echo $skip + 1 ?> to <?php 
                    if ( $skip + 1 + $page_limit > $count[0]['count'] ) {
                        
                        echo $count[0]['count'];
                    } else {
                        echo $skip + 1 + $page_limit;
                    }
                     ?> of <?php echo $count[0]['count']; ?> entries</div>
                </div>
                <?php if ( $total_pages > 1 ): ?>
                <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="basic-datatable_paginate">
                        <ul class="pagination pagination-rounded">
                            <?php if ( $page_no != 1 ):  ?>
                                <li class="paginate_button page-item previous" id="basic-datatable_previous">                                
                                    <a href="#" aria-controls="basic-datatable" data-dt-idx="0" onclick="getFilteredClassRoutine(<?php echo $page_no - 1; ?>)" tabindex="0" class="page-link">                                
                                        <i class="mdi mdi-chevron-left"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php
                            for ( $i = 1; $i <= $total_pages; $i++ ) {
                                echo '<li class="paginate_button page-item ' . ( $page_no == $i ? 'active' : '' ) . ' "><a href="#" onclick="getFilteredClassRoutine(' . $i . ')" aria-controls="basic-datatable" data-dt-idx="' . $i .  '" tabindex="0" class="page-link">' . $i . '</a></li>';
                            }
                            ?>                            
                            <?php if ( $page_no != $total_pages ):  ?>
                            <li class="paginate_button page-item next" id="basic-datatable_next"><a href="#" onclick="getFilteredClassRoutine(<?php echo $page_no + 1; ?>)" aria-controls="basic-datatable" data-dt-idx="7" tabindex="0" class="page-link"><i class="mdi mdi-chevron-right"></i></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
        </div>
<?php else : ?>
    <?php include APPPATH . 'views/backend/empty.php'; ?>
<?php endif; ?>

<?php
function displayDates($date1, $date2, $format = 'm/d/Y')
{
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while ($current <= $date2) {
        $dates[] = date($format, $current);
        $current = strtotime($stepVal, $current);
    }
    return $dates;
}

function prepareTimeSlot($startH, $startM)
{
    $stHour = $startH;
    $stMin = $startM;
    if ($stMin < 10) {
        $stMin = '0' . $stMin;
    }

    if ($stHour >= 12) {
        if ($stHour != 12)
            $stHour = $startH - 12;

        if ($startM == 0) {
            return $stHour . ' PM';
        } else {
            return $stHour . ':' . $stMin . ' PM';
        }
    } else {
        if ($startM == 0) {
            return $startH . ' AM';
        } else {
            return $startH . ':' . $stMin . ' AM';
        }
    }
}


?>
<script type="text/javascript">
    function onclickclose(elm, id) {
        if (!window.confirm("Do you really want to delete?")) {
            return;
        }

        $(elm).parent().remove();
        // Now ajax.
        $.ajax({
            url: "<?php echo route('section/deleteroutine/'); ?>",
            type: 'POST',
            data: {
                id: id
            },
            success: function(response) {

            }
        });
    }


    function viewComment( elm ) {
    var text = $(elm).next().text().trim() === "" ? "No comments about this Class from Teacher yet" : $(elm).next().text().trim();
    bootbox.alert({
        size: "small",
        title: "Comment by You",
        message: text,
        callback: function(){ /* your callback code */ }
    });   
}

function seerecording(url) {
    window.open(url, '_blank');
}
</script>

<style>
    .dropdown-toggle::after {
        display: none;
    }
</style>