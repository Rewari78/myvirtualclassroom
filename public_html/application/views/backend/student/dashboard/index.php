<?php
    $fromDate = date('m/d/Y');
    $toDate = displayDates($fromDate, 14);
    $nextDate = $toDate[1];
    $toDate = $toDate[count($toDate) - 1];       

    if ( !empty($_POST) ) {
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
    }


?>
<!-- start page title -->
<div class="row ">
<div class="col-xl-12">
    <div class="card">
    <div class="card-body">
        <h4 class="page-title"> <?php echo 'Today\'s Date - ' . date('jS F [l]'); ?> </h4>
    </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
</div>

<div class="row">
<div class="col-xl-12">
    <div class="card p-3">
        <form action="#" method="post">
        <div class="row">
            
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label ><?php echo 'From Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="fromdate" onchange="onfromdateChange(this.value);" value="<?php echo $fromDate; ?>" name="fromdate"  data-provide = "datepicker" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label><?php echo 'To Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="todate" name="todate" data-provide = "datepicker" data-date-start-date="<?php echo $fromDate;  ?>" required value="<?php echo $toDate; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" type="submit" ><?php echo get_phrase('Submit'); ?></button>
            </div>
            
        </div>
        </form>
    </div>
</div>
</div>
<!-- end page title -->
<?php 
$timestamp = strtotime($fromDate);
$nexttimestamp = strtotime($toDate);

$student = $this->db->query("SELECT * FROM `students` WHERE `user_id` = ?", [$this->session->userdata['user_id']])->result_array();
$student_id = count($student) >= 1 ? $student[0]['id'] : null;

$user_name = $this->db->query("SELECT * FROM `users` WHERE `id` = ?", [$this->session->userdata['user_id']])->result_array();
$user_name = count($user_name) >= 1 ? $user_name[0]['name'] : 'Unknown';

if ( !$student_id ) {
    echo "You are not a student.";exit;
}

// get student section and everything.
$SQL = "SELECT * FROM `enrols` WHERE `student_id` = ?";
$result = $this->db->query($SQL, [$student_id])->result_array();
$userData = $result[0];

$SQL = "SELECT * FROM `routines` AS `r` 
INNER JOIN `busy_sections` AS `b`
ON ( `b`.`routine_id` = `r`.`id` )
WHERE `r`.`class_id` = ? AND `r`.`school_id` = ? AND `r`.`rawdate` >= ? AND `r`.`rawdate` <= ?  AND `b`.`section_id` = ? GROUP BY `routine_id` ORDER BY `r`.`rawdate` ASC";
$routines  = $this->db->query($SQL, [$result[0]['class_id'], school_id(), $timestamp, $nexttimestamp, $result[0]['section_id']])->result_array();


// Set sections
$SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
 
$sections = array();

//GEt teachers
$SQL = "SELECT `t`.`id`, `u`.`id` AS `user_id`, `u`.`name` FROM `users` AS `u`
        INNER JOIN `teachers` AS `t`
        ON ( `u`.`id` = `t`.`user_id` )
        INNER JOIN `routines` AS `r`
        ON ( `r`.`teacher_id` = `t`.`id` )
        ";
$result = $this->db->query($SQL)->result_array();
$teachers = array();
foreach ( $result as $row )
{
    $teachers[$row['id']] = $row;
}

// Set sections
$SQL = "SELECT * FROM `common_subject`";
$result = $this->db->query($SQL)->result_array();
$subjects = array();
foreach( $result as $row )
{
    $subjects[$row['id']] = $row;
}

?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card p-3">
            <?php if ( count($routines) >= 1 ): ?>
            <?php
                $times = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id, 'type' => 0))->result_array();
                $firstTime = $routines[0]['date'];
                $dateList = displayDates2($firstTime, $toDate);

            ?>
            <div class="list-table">
        <table class="table">
        <?php $i = 1; ?>
        <?php foreach( $times as $time ): ?>
            
                <?php
                    $slot = prepareTimeSlot($time['start_time_hour'], $time['start_time_min']) .'-'. prepareTimeSlot($time['end_time_hour'], $time['end_time_min']);
                    $timeIndex[] = $slot;
                ?>
            
        <?php  endforeach; ?>                    
            <?php foreach ( $dateList as $date ): ?>
                
                <tr>
                    <td><?php                     
                    $day = date('l', strtotime($date));
                    echo $day . "<br />";
                    echo $date;

                    ?></td>

                    <?php


                    $dbValues = array($userData['class_id'], school_id(), $date, $userData['section_id']);
                    $SQL = "SELECT * FROM `routines` AS `r` 
                            INNER JOIN `busy_sections` AS `b`
                            ON ( `b`.`routine_id` = `r`.`id` )
                            WHERE `r`.`class_id` = ? AND `r`.`school_id` = ? AND `r`.`date` = ?  AND `b`.`section_id` = ? GROUP BY `routine_id` ORDER BY `r`.`rawdate` ASC";

                    $routines = $this->db->query($SQL, $dbValues)->result_array();                    

                    if ( count($routines) <= 0 ) {
                        echo '<td class="routine-gap text-center" colspan="' . count($timeIndex) . '">No Scheduled classes</td>';
                        continue;
                    }
                    ?>
                    <?php foreach( $timeIndex as $time ): ?>
                        
                            <?php
                                $dbValues = array($userData['class_id'], school_id(), $date, $userData['section_id'], $time);                                
                                $SQL = "SELECT `r`.* FROM `routines` AS `r` 
                                    INNER JOIN `busy_sections` AS `b`
                                    ON ( `b`.`routine_id` = `r`.`id` )
                                    WHERE `r`.`class_id` = ? AND `r`.`school_id` = ? AND `r`.`date` = ?  AND `b`.`section_id` = ? AND `r`.`htimeslot` = ? GROUP BY `routine_id` ORDER BY `r`.`rawdate` ASC";


                                $routines = $this->db->query($SQL, $dbValues)->result_array();                                
                        ?>
                        <td class="<?php echo (!empty($routines) ? '' : 'empty') ?>">
                        <?php
                            
                                foreach ( $routines as $routine )
                                {
                                    $teacherName = isset($teachers[$routine['teacher_id']]) ? $teachers[$routine['teacher_id']]['name'] : 'Unknown';
                                    $subjectName = isset($subjects[$routine['subject_id']]) ? $subjects[$routine['subject_id']]['name'] : 'Unknown';

                                    // Section detection
                                    // Set sections                                    
                                    $SQL = "SELECT * FROM `sections` WHERE `class_id` = ? AND `id` IN (" . $routine['section_id'] . ")";
                                    $result = $this->db->query($SQL, [$routine['class_id']])->result_array();
                                    $sections = array();
                                    foreach( $result as $row )
                                    {
                                        $sections[$row['id']] = $row;
                                    }

                                    $avlSections = array();
                                    foreach ( explode(',', $routine['section_id'])  as $id )
                                    {
                                        $avlSections[] = isset($sections[$id]) ? $sections[$id]['name'] : 'NA';
                                    }

                                    $avlSections = implode(',', $avlSections);                                    

                                    // Class declaration
                                    $SQL = "SELECT * FROM `classes` WHERE `id` = ? AND `school_id`  = ?";
                                    $result = $this->db->query($SQL, [$routine['class_id'], school_id()])->result_array();

                                    $sssssssssssss = count($result) >= 1 ? $result[0]['name'] : 'NA';
                                    $std = $sssssssssssss . ' - ';
                                    $std .= $avlSections;
                                    $tttt = ( strtotime($routine['date']) + ($routine['starting_hour'] * 60 * 60) + ($routine['starting_minute'] * 60)) * 1000;
                                    $endtt = ( strtotime($routine['date']) + ($routine['ending_hour'] * 60 * 60) + ($routine['ending_minute'] * 60)) * 1000;
                                    $duration = ( $endtt - $tttt ) / 1000 / 60;
                                    $isFinished = ($endtt / 1000) - time() <= 10 ? true : false;
                                    $meetings = $this->db->query("SELECT * FROM `meetings` WHERE `routine_id` = ?", [$routine['id']])->result_array();

                                    $output = "<strong>Time:</strong> $time<br /> <strong>Subject:</strong> $subjectName<br /><strong>Teacher:</strong> $teacherName<br /><strong>STD:</strong> $std<br />";
                                    $output .= ( !empty($routine['t_comment']) ? '<div class="comment-update" style="max-width: 173px;white-space: normal">' .  (mb_strlen($routine['t_comment']) > 33 ? mb_substr($routine['t_comment'], 0, 33) . '...' : $routine['t_comment']) . '</div>' : '');
                                    $output .= ( !empty($routine['t_comment']) ? '<a href="javascript:void(0)" style="color: white;text-decoration: underline;" onclick="viewComment(' . $routine["id"] . ', this);">View Comment</a>' : '' );
                                    $output .= ( !empty($routine['t_comment']) ? '<div class="hide comment-update">' .  $routine['t_comment'] . '</div>' : '');
                                    if ( !$isFinished ) {
                                        $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" onclick="joinClass(' . $routine['id'] . ', \'' . $user_name . '\', ' . $endtt . ', ' . $tttt . ', \'' . $sssssssssssss . '\', this)">Join Class</button>';                                        
                                    }

                                    $output .= '<br /><button type="button" class="btn btn-light mt-2" style="box-shadow: none;width: 100%;" id="startClass" name="startClass" onclick="viewFiles(' . $routine['id'] . ');">View Files</button>';
                                    
                                    echo '<div class="cal-slot">';
                                    echo $output;
                                    echo '</div>';
                                }
                            ?>
                        </td>
                        
                    <?php endforeach; ?>
                    
                </tr>                
            <?php endforeach; ?>
        </table>
    </div>

            <?php else: ?>
                No schedule found.
            <?php endif; ?>
        </div>
    </div><!-- end col-->
</div>

<script>
$(document).ready(function() {
    initDataTable("expense-datatable");
});

function viewComment( id, elm ) {
    var text = $(elm).next().text().trim() === "" ? "No comment yet" : $(elm).next().text().trim();
    bootbox.alert({
        size: "small",
        title: "Comment by Teacher",
        message: text,
        callback: function(){ /* your callback code */ }
    });   
}

function viewFiles( id )
{
    var waitModel = bootbox.dialog({
            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Please wait...</p>',
            closeButton: false,
            animate: false,
            onShow: function() {
                // Send the ajax for data
                $.ajax({
                    url: '<?php echo route('section/view_files/') ?>',
                    data: {
                        id: id,
                    },
                    type: 'POST',                    
                    success: function(response) {                                
                        waitModel.modal('hide');
                        bootbox.alert({
                            message: response,
                            closeButton: true,
                             buttons: {
                                ok: {
                                    label: 'close'
                                }
                            }
                        });
                    }
                });
            }
    });

}

function joinClass( id, name, endTime, startTime, std, elm ) {
    if ( endTime - new Date().getTime() < 10000 ) {
        $(elm).remove();
        return;
    }

    if ( std == 'VII' || std == 'X' ) {
        if ( startTime - new Date().getTime() > 240000 ) {
            bootbox.alert("You can join this Class just before or on alloted timing only.");    
            return;
        }
    } else if ( std == 'VI' || std == 'IX' ) {
        if ( startTime - new Date().getTime() > 120000 ) {
            bootbox.alert("You can join this Class just before or on alloted timing only.");    
            return;
        }

    } else if ( std == 'VIII' || std == 'V' ) {
        if ( startTime - new Date().getTime() > 0 ) {
            bootbox.alert("You can join this Class just before or on alloted timing only.");    
            return;
        }
    } else {
        if ( startTime - new Date().getTime() > 180000 ) {
            bootbox.alert("You can join this Class just before or on alloted timing only.");    
            return;
        }
    }

    

    
    
    elm.disabled = true;

    $.ajax({
        url: '<?php echo route('section/join/') ?>',
        data: {
            id: id,
            name: name
        },
        type: 'POST',
        success: function(response) {
            if ( response.success ) {


                //  send an ajax
                // to register
                // $.ajax({
                //     url: '<?php echo route('section/join/') ?>',
                //     data: {
                //         id: id,
                //         name: name
                //     },
                //     type: 'POST'                    
                // });

                window.open(response.join, '_blank');
                window.location.reload();


                
            } else {
                if ( startTime - new Date().getTime() > 180000 ) {
                    bootbox.alert("You can join this Class just before or on alloted timing only.");    
                }else if(response.join == "ALREADY_IN_MEETING"){
                    bootbox.alert(response.Message);  
                }else {
                    bootbox.alert("Teacher have not started the Class Yet.");    
                }
                
            }            
        },
        complete: function() {
          window.location.reload();
        }
    });
}

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}
</script>

<?php

function displayDates($date1, $upto, $format = 'm/d/Y' )
{
    $dates = array();
    $current = strtotime($date1);  
    $stepVal = '+1 day';
    $i = 0;
    while( $i < $upto ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
       $i++;
    }
    return $dates;
}

  function displayDates2($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }



function dateGap($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return count($dates);
 }


function prepareTimeSlot( $startH, $startM ) 
{
   $stHour = $startH;
   $stMin = $startM;
   if($stMin<10){
       $stMin = '0'.$stMin;
   }
   
   if($stHour >=12){
       if($stHour != 12)
       $stHour = $startH - 12;
       
       if($startM == 0){
           return $stHour.' PM'; 
       }else{
           return $stHour.':'.$stMin.' PM'; 
       }
       
   }
   else{
       if($startM == 0){
           return $startH. ' AM'; 
       }else{
           return $startH.':'.$stMin. ' AM'; 
       }
   
   }
}


