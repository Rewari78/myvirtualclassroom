<?php

$result = $this->db->query("SELECT * FROM `routines` WHERE `id` = ? ", [$routine_id])->result_array();
$routine = $result[0];

$t_files = json_decode($routine['t_files'], true);
$t_files = is_array($t_files) ? $t_files : array();

if ( empty($t_files) ) {
    echo "No files yet uploaded by the teacher.";
    exit;
}

?>

<div class="">
    <?php foreach ( $t_files as $file ): ?>
        <div class="mb-3">
        <a href="<?php echo site_url('uploads/teacher_files/' . $file['file']) ?>" target="_blank" style="text-decoration: none;">
                <strong><?php echo $file['name'] ?></strong>
        </a>
        </div>
    <?php  endforeach; ?>
    
    
</div>