<?php
    $fromDate = '';    
    $toDate = '';       

    if ( !empty($_POST) ) {
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
    }


?>
<!-- start page title -->
<div class="row ">
<div class="col-xl-12">
    <div class="card">
    <div class="card-body">
        <h4 class="page-title"> <?php echo 'Time Tablewise'; ?> </h4>
    </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
</div>
<!-- end page title -->

<?php

$student = $this->db->query("SELECT * FROM `students` WHERE `user_id` = ?", [$this->session->userdata['user_id']])->result_array();
$student_id = count($student) >= 1 ? $student[0]['id'] : null;

if ( !$student_id ) {
    echo "You are not a student.";
}

$SQL = "SELECT * FROM `enrols` WHERE `student_id` = ?";
$result = $this->db->query($SQL, [$student_id])->result_array();
$userData = $result[0];

// GET ALL SUBJECTS
$SQL = "SELECT * FROM `subjects` WHERE `class_id` = ? AND `section_id` = ? AND `school_id` = ? GROUP BY `common_subject_id` ORDER BY `name` ";
$result = $this->db->query($SQL, [$userData['class_id'], $userData['section_id'], school_id()])->result_array();
$subjects = array();
foreach ($result as $row) {
    $subjects[$row['common_subject_id']] = $row;
}

?>
<div class="row">
<div class="col-xl-12">
    <div class="card p-3">
        <form action="#" method="post">
        <div class="row">
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label ><?php echo 'Select Subject'; ?></label>
                    <div >
                        <select type="text" class="form-control" id="subject_id" name="subject_id">
                        <option value=""><?php echo "Select A Subject"; ?></option>
                            <?php foreach ( $subjects as $key => $subject ): ?>
                                <option value="<?php echo $key; ?>"><?php echo $subject['name']; ?></option>
                            <?php  endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label ><?php echo 'From Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="fromdate" onchange="onfromdateChange(this.value);" value="<?php echo $fromDate; ?>" name="fromdate"  data-provide = "datepicker" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-1">
                <div class="form-group">
                    <label><?php echo 'To Date'; ?></label>
                    <div >
                        <input type="text" class="form-control" id="todate" name="todate" data-provide = "datepicker" data-date-start-date="<?php echo $fromDate;  ?>" required value="<?php echo $toDate; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" type="button" onclick="filter_class_routine();" ><?php echo get_phrase('Submit'); ?></button>
            </div>
            
        </div>
        </form>

        <div class="card-body class_routine_content">
				<?php include 'list.php'; ?>
			</div>
    </div>
</div>

<script>

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}

function filter_class_routine()
{
    var from_date = $('#fromdate').val();
	var to_date = $('#todate').val();
	var subject_id = $('#subject_id').val();		
	
	if ( !from_date || !to_date || !subject_id  ) {
		toastr.error('Please select Subject, From Date and To Date');
		return;
	};

	getFilteredClassRoutine( 1, from_date, to_date, subject_id );
}

var getFilteredClassRoutine = function( pageNo, from_date, to_date, subject_id ) {

		$.ajax({
			url: '<?php echo route('section/filter/') ?>',
			data: {
				from_date: from_date,
                to_date: to_date,
                subject_id: subject_id,
                pageNo: pageNo
			},
			type: 'POST',
			success: function(response) {
				$('.class_routine_content').html(response);
			}
		});
	
}
</script>

<?php

function displayDates($date1, $upto, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);  
    $stepVal = '+1 day';
    $i = 0;
    while( $i < $upto )
    {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
       $i++;
    }
    return $dates;
}