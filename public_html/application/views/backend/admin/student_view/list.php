<?php
$school_id = school_id();
$check_data = $this->db->get_where('users', array('school_id' => $school_id, 'role' => 'student'));

// Set sections
$SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
 
$sections = array();

$result = $this->db->query($SQL, [$_GET['classId']])->result_array();
foreach ( $result as $row ) {
    $sections[$row['id']] = $row;
}

$SQL = "SELECT * FROM `classes` WHERE `school_id` = ?";
 
$classes = array();

$result = $this->db->query($SQL, [$school_id])->result_array();
foreach ( $result as $row ) {
    $classes[$row['id']] = $row;
}

if($check_data->num_rows() > 0):?>
<table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%">
    <thead>
        <tr style="background-color: #313a46; color: #ababab;">
            <th>Staff ID</th>
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo 'Class'; ?></th>
            <th><?php echo 'Section'; ?></th>
            <th><?php echo 'Mobile'; ?></th>
            <th>Web UserID</th>
            <th>Web Password</th>
            <th>Status</th>
            <th></th>
            <th>Action</th>
            <!-- <th><?php // echo get_phrase('options'); ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php
        $SQL = "SELECT `u`.*, `e`.`class_id`, `e`.`section_id` FROM `users` AS `u`
        INNER JOIN `students` AS `s`
        ON ( `s`.`user_id` = `u`.`id` )
        INNER JOIN `enrols` AS `e`
        ON ( `e`.`student_id` = `s`.`id` )
        WHERE `u`.`school_id` = ?
        AND `u`.`role` = 'student'
        AND `e`.`class_id` = ?
        AND `e`.section_id IN (" .  implode(", ", json_decode($_GET['sections'], true)) . ")";

        $class_id = $_GET['classId'];
        $students = $this->db->query($SQL, [$school_id, $class_id])->result_array();
        foreach($students as $student){
            ?>
            <tr>
                <td><?php echo $student['id'];  ?></td>
                <td><?php echo $student['name']; ?></td>
                <td><?php
                echo isset($classes[$student['class_id']]) ? $classes[$student['class_id']]['name'] : 'NA';                
                ?></td>
                <td><?php 
                
                echo isset($sections[$student['section_id']]) ? $sections[$student['section_id']]['name'] : 'NA';                
                
                ?></td>
                <td><?php echo $student['phone']; ?></td>
                <td><?php echo $student['WEB_USER_ID']; ?></td>
                <td><?php echo $student['WEB_PASSWORD']; ?></td>
                <td><?php 
                if($student['status'] == 1){
                    echo "Active";
                }else{
                    echo "<p style=color:red;>Disabled</p>";
                }
                ?></td>
                <td><?php 
                if($student['status'] == 1){
                    ?><a href="javascript:void(0);" class="dropdown-item" onclick="confirmModal('<?php  echo route('student_view/disable/'.$student['id']); ?>', showAllTeachers )"><?php  echo 'Click here to Disable'; ?></a>
                    <?php
                }else{
                     ?><a href="javascript:void(0);" class="dropdown-item" onclick="confirmModal('<?php  echo route('student_view/active/'.$student['id']); ?>', showAllTeachers )"><?php  echo 'Click here to Activate'; ?></a>
                    <?php
                }
                ?></td>
                <td>
                    <div class="dropdown text-center">
                        <button type="button" class="btn btn-sm btn-icon btn-rounded btn-outline-secondary dropdown-btn dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-vertical"></i></button>
                        <div class="dropdown-menu dropdown-menu-right">                            
                            <a href="javascript:void(0);" class="dropdown-item" onclick="rightModal('<?php echo site_url('admin/student_view/edit/'.$student['id']); ?>', 'Edit Student')">Edit</a>                            
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php else: ?>
    <?php include APPPATH.'views/backend/empty.php'; ?>
<?php endif; ?>


