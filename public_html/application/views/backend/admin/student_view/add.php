<?php $school_id = school_id(); ?>
<form method="POST" class="d-block ajaxForm" action="<?php echo route('student_view/create_ajax'); ?>">
  <div class="form-row">

    <input type="hidden" name="school_id" value="<?php echo school_id(); ?>">
    <input type="hidden" name="session" value="<?php echo active_session();?>">

    <div class="form-group col-md-12">
      <label for="class_id_on_create">Full Name</label>
      <input type="text" class="form-control" name="fname" required/>     
    </div>

    <div class="form-group col-md-12">
      <label for="class_id_on_create">Phone</label>
      <input type="text" class="form-control" name="phone" required/>     
    </div>

    <div class="form-group col-md-12">
      <label for="class_id_on_create">Gender</label>
      <select name="gender" class="form-control select2" data-toggle = "select2"  required>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>

    <div class="form-group col-md-12">
      <label for="class_id_on_create"><?php echo get_phrase('class'); ?></label>
      <select name="class_id" id="class_id_on_create" class="form-control select2" data-toggle="select2" onchange="classWiseSectionModel(this.value)" required>
        <option value=""><?php echo get_phrase('select_a_class'); ?></option>
        <?php
        $classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
        foreach($classes as $class){
          ?>
          <option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
        <?php } ?>
      </select>
      <small id="class_help" class="form-text text-muted"><?php echo get_phrase('select_a_class'); ?></small>
    </div>
    
    
     <div class="form-group col-md-12">
      <label for="class_id_on_create"><?php echo get_phrase('section'); ?></label>
      <select name="section" id="section_id" class="form-control select2" data-toggle = "select2"  required>
                        <option value=""><?php echo get_phrase('select_a_section'); ?></option>
                    </select>
     
    </div>


    <div class="form-group col-md-12">
      <label for="class_id_on_create">WEB USER ID</label>
      <input type="text" class="form-control" name="id" required/>     
    </div>

    <div class="form-group col-md-12">
      <label for="class_id_on_create">WEB PASSWORD</label>
      <input type="text" class="form-control" name="password" required/>     
    </div>
    
    <div class="form-group  col-md-12">
      <button class="btn btn-block btn-primary" type="submit"><?php echo 'SAVE'; ?></button>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
  initSelect2(['#class_id_on_create']);
  initSelect2(['#sub_id_on_create']);
   initSelect2(['#section_id']);
    initSelect2(['#teacher_id']);
});

function classWiseSectionModel(classId) {
        $.ajax({
            url: "<?php echo route('section/list/'); ?>"+classId,
            success: function(response){
                console.log(response);
                var count = (response.match(/option/g) || []).length;
                //we are adding ALL when there are more then one section
                if(count>2){                    
                     $('#section_id').html(response);
                }else{
                     $('#section_id').html(response);
                }
              
               
            }
        });
    }


$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
  var form = $(this);
  ajaxSubmit(e, form, function() {
    showAllTeachers();
  });
});
</script>
