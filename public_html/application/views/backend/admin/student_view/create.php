<form method="POST" class="d-block ajaxForm" action="<?php echo route('staff/create'); ?>" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-md-12">
            <input type="hidden" name="school_id" value="<?php echo school_id(); ?>">
            <label for="name"><?php echo 'Staff Name'; ?></label>
            <input type="text" class="form-control" id="name" name = "name" required>
            
        </div>


     <div class="form-group col-md-12">
            <label for="phone"><?php echo get_phrase('phone_number'); ?></label>
            <input type="number" class="form-control" id="phone" name = "phone" required>
        </div>
       
       
        <div class="form-group col-md-12">
            <label for="gender"><?php echo 'Select Role'; ?></label>
            <select name="role" id="role" class="form-control select2" data-toggle = "select2" required>
                <option value=""><?php echo 'Select Role'; ?></option>
                <option value="teacher"><?php echo 'Teaching Staff'; ?></option>
                <option value="admin"><?php echo 'Admin'; ?></option>
                <option value="principal"><?php echo 'Principal / Management'; ?></option>
            </select>
        </div>

        

        <div class="form-group mt-2 col-md-12">
            <button class="btn btn-block btn-primary" type="submit"><?php echo 'Create Staff'; ?></button>
        </div>
    </div>
</form>

<script>
$(document).ready(function () {
    initSelect2([ '#role']);
});
$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
    var form = $(this);
    ajaxSubmit(e, form, showAllTeachers);
});

// initCustomFileUploader();
</script>
