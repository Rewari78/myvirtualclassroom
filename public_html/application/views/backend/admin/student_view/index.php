<!--title-->
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title">
          <i class="mdi mdi-account-circle title_icon"></i> <?php echo 'Students'; ?>
          <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle" onclick="rightModal('<?php echo site_url('admin/student_view/create'); ?>', '<?php echo 'Add Student'; ?>')"> <i class="mdi mdi-plus"></i> <?php echo 'Add Student'; ?></button>
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
	<div class="col-12">
    <div class="card">
			<div class="row mt-3  pl-3 pr-3 top-level">	
				
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label ><?php echo 'Class'; ?></label>
						<select name="class" id="class_id" class="form-control select2" data-toggle="select2" required onchange="classWiseSection(this.value)">
							<option value=""><?php echo get_phrase('select_a_class'); ?></option>
							<?php
							$classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
							foreach($classes as $class){
								?>
								<option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>


				<div class="col-md-4 mb-1 section-combo">
					<div class="form-group">
						<label><?php echo 'Sections'; ?></label>												
						<div class="section-combo-list">
							Select class first
						</div>
					</div>
                </div>

            </div>
      </div>
    </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body teacher_content">
              Please select class and section first.
        <!-- <?php // include 'list.php'; ?> -->
      </div>
    </div>
  </div>
</div>

<script>
var showAllTeachers = function () {
  var url = '<?php echo route('student_view/list'); ?>';

  var classId = $('#class_id').val();
  var values = [];
  $('.section-input').each(function(i, v) {
    if ( v.checked ) {
      values.push(v.value);
    }
  });

  if ( values.length  >= 1 )
  {
    $.ajax({
      type : 'GET',
      url: url,
      data: {
        classId: classId,
        sections: JSON.stringify(values)
      },
      success : function(response) {
        $('.teacher_content').html(response);
        initDataTable('basic-datatable');
      }
    });
  } else {
    $('.teacher_content').html('Please select class and section first.');
  }


}


function classWiseSection(classId) {
	if ( classId )	
	{
		$.ajax({
		url: "<?php echo route('section/checkboxlist/'); ?>" + classId,
		data: {
			classId: classId
		},
		type: 'POST',
		success: function(response){
      console.log(response, $('.section-combo-list'));
			$('.section-combo-list').html(response);
		}
		});
	} else {
		$('.section-combo-list').html('Select class first');
    $('.teacher_content').html('Please select class and section first.');
	}
	
}

function onchangeSection() 
{
  showAllTeachers();
}
</script>
