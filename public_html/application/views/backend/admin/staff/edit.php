<?php
$users = $this->db->get_where('users', array('id' => $param1))->result_array();
foreach($users as $user):
  $teacher = $this->db->get_where('teachers', array('user_id' => $user['id']))->row_array();
  $social_links = json_decode($teacher['social_links'], true);
  ?>
  <form method="POST" class="d-block ajaxForm" action="<?php echo route('staff/update/'.$param1); ?>">
    <div class="form-row">
      
      
          <div class="form-group col-md-12">
            <input type="hidden" name="school_id" value="<?php echo school_id(); ?>">
            <label for="name"><?php echo 'Staff Name'; ?></label>
            <input type="text" class="form-control" id="name" name = "name" value= "<?php echo $user['name'];?>" required>
            
        </div>


     <div class="form-group col-md-12">
            <label for="phone"><?php echo get_phrase('phone_number'); ?></label>
            <input type="number" class="form-control" id="phone" name = "phone" value= "<?php echo $user['phone'];?>" required>
        </div>
       
       

    

      <div class="form-group mt-2 col-md-12">
        <button class="btn btn-block btn-primary" type="submit"><?php echo 'Update'; ?></button>
      </div>
    </div>
  </form>
<?php endforeach; ?>

<script>

$(document).ready(function () {
  initSelect2(['#department', '#gender', '#blood_group', '#show_on_website']);
});
$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
  var form = $(this);
  ajaxSubmit(e, form, showAllTeachers);
});

// initCustomFileUploader();
</script>
