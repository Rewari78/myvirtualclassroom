<?php
$school_id = school_id();
$check_data = $this->db->get_where('users', array('school_id' => $school_id, 'role' => 'teacher'));
if($check_data->num_rows() > 0):?>
<table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%">
    <thead>
        <tr style="background-color: #313a46; color: #ababab;">
            <th>Staff ID</th>
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo 'Role'; ?></th>
            <th><?php echo 'Mobile'; ?></th>
            <th>Web UserID</th>
            <th>Web Password</th>
            <th><?php echo get_phrase('options'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $teachers = $this->db->get_where('users', array('school_id' => $school_id, 'role' => 'teacher'))->result_array();
        foreach($teachers as $teacher){
            ?>
            <tr>
                <td><?php echo $teacher['id'];  ?></td>
                <td><?php echo $teacher['name']; ?></td>
                <td><?php echo $teacher['role']; ?></td>
                <td><?php echo $teacher['phone']; ?></td>
                <td><?php echo $teacher['WEB_USER_ID']; ?></td>
                <td><?php echo $teacher['WEB_PASSWORD']; ?></td>
                <td>
                    <div class="dropdown text-center">
                        <button type="button" class="btn btn-sm btn-icon btn-rounded btn-outline-secondary dropdown-btn dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-vertical"></i></button>
                        <div class="dropdown-menu dropdown-menu-right">
                             <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item" onclick="rightModal('<?php echo site_url('modal/popup/staff/edit/'.$teacher['id']); ?>', '<?php echo 'Update'; ?>')"><?php echo get_phrase('edit'); ?></a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item" onclick="confirmModal('<?php echo route('staff/delete/'.$teacher['id']); ?>', showAllTeachers )"><?php echo get_phrase('delete'); ?></a>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php else: ?>
    <?php include APPPATH.'views/backend/empty.php'; ?>
<?php endif; ?>
