<!--title-->
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title">
					<i class="mdi mdi-calendar-today title_icon"></i> <?php echo 'Copy Weekly Time Table'; ?>					
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="row mt-3  pl-3 pr-3 top-level">	
				
				<div class="col-md-6 mb-1">
					<div class="form-group">
						<label ><?php echo 'Class'; ?></label>
						<select name="class" id="class_id" class="form-control select2" data-toggle="select2" required onchange="classWiseSection(this.value)">
							<option value=""><?php echo get_phrase('select_a_class'); ?></option>
							<?php
							$classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
							foreach($classes as $class){
								?>
								<option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>


				<div class="col-md-6 mb-1 section-combo">
					<div class="form-group">
						<label><?php echo 'Sections'; ?></label>												
						<div class="section-combo-list">
							Select class first
						</div>
					</div>
				</div>

				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label ><?php echo 'Copy From'; ?></label>
						<div >
							<p class="copy-date-label">Select Week Dates - </p>
							<input type="text" value="" class="form-control" id="fromdate" name = "fromdate" data-provide = "datepicker" onchange="onchangeFromDate()" required>							
							<a href="#" class="view_table_c hide" onclick="onclickView()">View Timetable</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label><?php echo 'Copy To'; ?></label>
						<div class="copy-to-date">
								Select Copy From First
						</div>
					</div>
				</div>

					<!-- <input type="text" value="" class="form-control" id="date" name = "date" data-provide="datepicker" data-date-start-date="<?php // echo date('m/d/Y', time()); ?>"  required onchange="onchangedate();">
					<select name="section" id="section_id" class="form-control select2" data-toggle="select2" required>
						<option value=""><?php // echo get_phrase('select_section'); ?></option>
					</select> -->				
				<div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" onclick="onPressCopy()" ><?php echo get_phrase('Copy'); ?></button>
				</div>
			</div>
			<div class="card-body class_routine_content">
				
			</div>
		</div>
	</div>
</div>

<script>

function classWiseSection(classId) {
	if ( classId )	
	{
		$.ajax({
		url: "<?php echo route('section/sectioncombo/'); ?>",
		data: {
			classId: classId
		},
		type: 'POST',
		success: function(response){
			$('.section-combo-list').html(response);
		}
		});
	} else {
		$('.section-combo-list').html('Select class first');
	}
	
}

function onclickView()
{
	var classId = $('#class_id').val();
	var fromDate = $('#fromdate').val();
	var todate = $('#todate').val();
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
		if ( v.checked ) {
			section_ids.push(v.value);
		}
	});

	if ( !classId || !fromDate || section_ids.length <= 0 ) {
		toastr.error('Please select Class, Section, Copy From to View Timetable');
		return;
	};

	getFilteredClassRoutine();
}


function onclickView2()
{
	var classId = $('#class_id').val();
	var fromDate = $('#todate').val();
	var todate = $('#todate').val();
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
		if ( v.checked ) {
			section_ids.push(v.value);
		}
	});

	if ( !classId || !fromDate || section_ids.length <= 0 ) {
		toastr.error('Please select Class, Section, Copy From to View Timetable');
		return;
	};

	getFilteredClassRoutine2();
}

function onchangeToDate() {
	var val = $('#todate').val();
	if ( !val )
	{
		$('.view_table_cc').addClass('hide');
		return;
	}

	$('.view_table_cc').removeClass('hide');	
}

function onchangeFromDate() {
	var val = $('#fromdate').val();
	if ( !val )
	{
		$('.copy-to-date').html('Select Copy From First');		
		$('.view_table_c').addClass('hide');
		return;
	}

	$('.view_table_c').removeClass('hide');	

	var date = new Date(val);
	date = date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));
	date = new Date(date);
	console.log(date);
	var month  = date.getMonth() + 1;
	month = month < 10 ? "0" + month : month.toString();
	var day  = date.getDate();
	day = day < 10 ? "0" + day : day.toString();
	var year = date.getFullYear();

	var dd = [0, 1, 2, 3, 4, 5, 6];
	dd.remove(date.getDay());
	dd = dd.join(',');
	var str = '<p class="copy-date-label">Select Week First Date - </p><input  onchange="onchangeToDate()" type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + month + '/' + day + '/' + year + '" data-date-days-of-week-disabled="' + dd + '" required><p class="small">Date should be same as <strong>Copy From</strong><br /><a href="#" class="view_table_cc hide" onclick="onclickView2()">View Timetable</a></p>';
	$('.copy-to-date').html(str);
}

function filter_class_routine(){
	var class_id = $('#class_id').val();	
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
		if ( v.checked ) {
			section_ids.push(v.value);
		}
	});

	if(class_id != "" && section_ids.length > 0 ){		
		getFilteredClassRoutine();
	}else{
		toastr.error('<?php echo get_phrase('please_select_a_class_and_section'); ?>');
	}
}

function onPressCopy()
{
	var classId = $('#class_id').val();
	var fromDate = $('#fromdate').val();
	var todate = $('#todate').val();
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
        if ( v.checked ) {
			section_ids.push(v.value);
        }
	});

	if ( !classId || !fromDate || !todate || section_ids.length <= 0 ) {
		toastr.error('Please select Class, Copy From, Copy To and Section Ids to copy.');
		return;
	};


var dialog = bootbox.dialog({
	message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Please wait while we copy...</p>',
	closeButton: false
});


doCopy(classId, fromDate, todate, section_ids, true, function() {
	// do something in the background
	dialog.modal('hide');
	window.location.reload();
	// window.location.href = window.location.href.replace('#', '') + "?copydone=1";
});
	
}

function doCopy ( classId, fromDate, toDate, section_ids, overwrite, callback ) {
	$.ajax({
        url: "<?php echo route('section/checkcopyslot/'); ?>",
        type: 'POST',
        data: {
            classId: classId,
			fromDate: fromDate,
			toDate: toDate,
			sections: JSON.stringify(section_ids),
			overwrite: overwrite ? "true" : "false"
        },
        success: function(response){            
			callback()
            // notice();
        }
    });
}

var getFilteredClassRoutine = function() {
	var class_id = $('#class_id').val();
	var from_date = $('#fromdate').val();


	var date = new Date(from_date);
	date = date.setTime(date.getTime() + (6 * 24 * 60 * 60 * 1000));
	date = new Date(date);
	var month  = date.getMonth() + 1;
	month = month < 10 ? "0" + month : month.toString();
	var day  = date.getDate();
	day = day < 10 ? "0" + day : day.toString();
	var year = date.getFullYear();

	var to_date = month + "/" + day + "/" + year;

	
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
		if ( v.checked ) {
			section_ids.push(v.value);
		}
	});

	if ( section_ids.length <= 0 ) return;
	
	if(class_id != "" && from_date!= "" && to_date != ""){
		$.ajax({
			url: '<?php echo route('create_timetable/filter/') ?>',
			data: {
				classId: class_id,
				fromDate: from_date,
				toDate: to_date,
				sectionIds: JSON.stringify(section_ids)
			},
			type: 'POST',
			success: function(response){
				$('.class_routine_content').html(response);
			}
		});
	}
}


var getFilteredClassRoutine2 = function() {
	var class_id = $('#class_id').val();
	var from_date = $('#todate').val();


	var date = new Date(from_date);
	date = date.setTime(date.getTime() + (6 * 24 * 60 * 60 * 1000));
	date = new Date(date);
	var month  = date.getMonth() + 1;
	month = month < 10 ? "0" + month : month.toString();
	var day  = date.getDate();
	day = day < 10 ? "0" + day : day.toString();
	var year = date.getFullYear();

	var to_date = month + "/" + day + "/" + year;

	
	var section_ids = [];	

	$('.combo-section-input').each(function(i, v) {
		if ( v.checked ) {
			section_ids.push(v.value);
		}
	});

	if ( section_ids.length <= 0 ) return;
	
	if(class_id != "" && from_date!= "" && to_date != ""){
		$.ajax({
			url: '<?php echo route('create_timetable/filter/') ?>',
			data: {
				classId: class_id,
				fromDate: from_date,
				toDate: to_date,
				sectionIds: JSON.stringify(section_ids)
			},
			type: 'POST',
			success: function(response){
				$('.class_routine_content').html(response);
			}
		});
	}
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
</script>
