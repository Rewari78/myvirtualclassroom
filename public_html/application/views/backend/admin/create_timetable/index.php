<!--title-->
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title">
					<i class="mdi mdi-calendar-today title_icon"></i> <?php echo 'Create Time Table'; ?>
					<!-- <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle" class="create-routine-btn" onclick="rightModal('<?php // echo site_url('modal/popup/create_timetable/create'); ?>', '<?php echo 'Create Time Table'; ?>')"> <i class="mdi mdi-plus"></i> <?php echo 'Create Time Table'; ?></button> -->
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
	<div class="col-md-12">
	<div class="card p-3">
		<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
		<form method="POST" class="d-block ajaxForm" action="#" style="min-width: 300px;">
			<?php $school_id = school_id(); ?>
			<div class="form-group row">
				<label for="class_id_on_routine_creation" class="col-md-3 col-form-label"><?php echo 'Select Date'; ?></label>
				<div class="col-md-9">
					<input type="text" value="" class="form-control" id="date" name = "date" data-provide = "datepicker" data-date-start-date="<?php echo date('m/d/Y', time()); ?>"  required onchange="onchangedate();">
				</div>
			</div>
			
			
				<div class="form-group row">
					<label for="class_id_on_routine_creation" class="col-md-3 col-form-label"><?php echo get_phrase('class'); ?></label>
					<div class="col-md-9">
						<select name="class_id" id="class_id_on_routine_creation" class="form-control select2" data-toggle="select2"  required onchange="classWiseSectionForRoutineCreate(this.value)">
							<option value=""><?php echo get_phrase('select_a_class'); ?></option>
							<?php $classes = $this->db->get_where('classes', array('school_id' => $school_id))->result_array(); ?>
							<?php foreach($classes as $class): ?>
								<option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				
				<div class="form-group row hide section-row">
					<label for="section_id_on_routine_creation" class="col-md-3 col-form-label"><?php echo 'Section'; ?></label>
					<div class="col-md-9" id="sectionDiv">
						
					</div>
				</div>

				<div class="form-group row hide subject-row ">
					<label for="subject_id_on_routine_creation" class="col-md-3 col-form-label"><?php echo get_phrase('subject'); ?></label>
					<div class="col-md-9">
						<select name="subject_id" id = "subject_id_on_routine_creation" class="form-control select2" data-toggle="select2"  required onchange="onchangeSubject();">                
						</select>
					</div>
				</div>

				<div class="form-group row hide teacher-row">
					<label for="teacher" class="col-md-3 col-form-label"><?php echo get_phrase('teacher'); ?></label>
					<div class="col-md-9">
						<select name="teacher_id" id = "teacher_on_routine_creation" class="form-control select2" data-toggle="select2"  onchange="onchangeTeacher()" required>
							<option value=""><?php echo get_phrase('assign_a_teacher'); ?></option>               
						</select>
					</div>
				</div>


				<div class="form-group row hide timing-row">
					<label for="teacher" class="col-md-3 col-form-label"><?php echo 'Timing'; ?></label>
					<div class="col-md-9">
						<select name="timing" id = "timing" class="form-control select2" data-toggle="select2"  onchange="onchangeTimeSlot()" required>
							<option value=""><?php echo 'Choose Timing'; ?></option>              
						</select>
					</div>
				</div>

				<div class="section-notice hide">
				</div>

				<input type="hidden" name="hidden_value" id="hidden_value" value="0"/>

				<div class="form-group  col-md-12">
					<button class="btn btn-block btn-primary" onclick="onclickContinue();" type="button"><?php echo 'Save & Continue'; ?></button>
				</div>
				<!-- <div class="form-group  col-md-12"> -->
					<!-- <button class="btn btn-block btn-primary" type="submit"><?php // echo 'Save & Close'; ?></button> -->
				<!-- </div> -->

				<div class="form-group  col-md-12">
					<button class="btn btn-block btn-primary" type="button" onclick="onclickclose()"><?php echo 'Reset'; ?></button>
				</div>
		</form>


		</div>
		<div class="col-md-3"></div>
		</div>
	</div>
	</div>
</div>

<div class="row hide">
	<div class="col-12">
		<div class="card">
			<div class="row mt-3  pl-3 pr-3 top-level">	
				
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label ><?php echo 'Class'; ?></label>
						<select name="class" id="class_id" class="form-control select2" data-toggle="select2" required onchange="classWiseSection(this.value)">
							<option value=""><?php echo get_phrase('select_a_class'); ?></option>
							<?php
							$classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
							foreach($classes as $class){
								?>
								<option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label ><?php echo 'From Date'; ?></label>
						<div >
							<input type="text" value="" class="form-control" id="fromdate" onchange="onfromdateChange(this.value);" name = "fromdate" data-provide = "datepicker" required>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-1">
					<div class="form-group">
						<label><?php echo 'To Date'; ?></label>
						<div >
							<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" required>
						</div>
					</div>
				</div>

				<div class="col-md-4 mb-1 section-combo">
					<div class="form-group">
						<label><?php echo 'Sections'; ?></label>												
						<div class="section-combo-list">
							Select class first
						</div>
					</div>
				</div>
					<!-- <input type="text" value="" class="form-control" id="date" name = "date" data-provide="datepicker" data-date-start-date="<?php // echo date('m/d/Y', time()); ?>"  required onchange="onchangedate();">
					<select name="section" id="section_id" class="form-control select2" data-toggle="select2" required>
						<option value=""><?php // echo get_phrase('select_section'); ?></option>
					</select> -->				
				<div class="col-md-2 pt-3">
					<button class="btn btn-block btn-secondary" onclick="filter_class_routine()" ><?php echo get_phrase('filter'); ?></button>
				</div>
			</div>
			<div class="card-body class_routine_content">
				<?php include 'list.php'; ?>
			</div>
		</div>
	</div>
</div>

<script>

function onfromdateChange( val ) {
		$('#todate').val(val).replaceWith('<input type="text" value="" class="form-control" id="todate" name = "todate" data-provide = "datepicker" data-date-start-date="' + val + '" required>');
}

function classWiseSection(classId) {
	if ( classId )	
	{
		$.ajax({
		url: "<?php echo route('section/sectioncombo/'); ?>",
		data: {
			classId: classId
		},
		type: 'POST',
		success: function(response){
			$('.section-combo-list').html(response);
		}
		});
	} else {
		$('.section-combo-list').html('Select class first');
	}
	
}

function filter_class_routine()
{
	var class_id = $('#class_id').val();
	var from_date = $('#fromdate').val();
	var to_date = $('#todate').val();
	var values = [];
    $('.combo-section-input').each(function(i, v) {
        if ( v.checked ) {
            values.push(v.value);
        }
	});
	
	if ( !class_id || !from_date || !to_date || values.length <= 0  ) {
		toastr.error('Please select Class, From Date, To Date and Sections to Filter');
		return;
	};

	getFilteredClassRoutine();
}

var getFilteredClassRoutine = function() {
	var class_id = $('#class_id').val();
	var from_date = $('#fromdate').val();
	var to_date = $('#todate').val();

    var values = [];
    $('.combo-section-input').each(function(i, v) {
        if ( v.checked ) {
            values.push(v.value);
        }
	});
	
	if ( values.length <= 0  ) return;
	
	if(class_id != "" && from_date!= "" && to_date != "") {
		$.ajax({
			url: '<?php echo route('create_timetable/filter/') ?>',
			data: {
				classId: class_id,
				fromDate: from_date,
				toDate: to_date,
				sectionIds: JSON.stringify(values)
			},
			type: 'POST',
			success: function(response) {
				$('.class_routine_content').html(response);
			}
		});
	}
}

$(document).ready(function () {

initSelect2(['#class_id_on_routine_creation',
'#section_id_on_routine_creation',
'#subject_id_on_routine_creation',
'#teacher_on_routine_creation',
'#class_room_id_on_routine_creation',
'#day_on_routine_creation',
'#starting_hour_on_routine_creation',
'#starting_minute_on_routine_creation',
'#ending_hour_on_routine_creation',
'#ending_minute_on_routine_creation',
'#timing']);
});

// $(".ajaxForm").validate({}); // Jquery form validation initialization

function  submit() {


	var val =  $('.timing-row select').val();
	var date = $('#date').val();
	var class_id = $('#class_id_on_routine_creation').val();
	var subject_id = $('#subject_id_on_routine_creation').val();
	var teacher_id = $('#teacher_on_routine_creation').val();

	var values = [];
	$('.section-input').each(function(i, v) {
		if ( v.checked ) {
			values.push(v.value);
		}
	});

	if ( !val || val == "-1" || val == "0" || !date || !subject_id || !teacher_id ) {
		toastr.error("Please fill the form before save.");
		return false;
	}


	var form = $('.ajaxForm');
	_customAjaxSubmit(form, getFilteredClassRoutine);


	return false;
}

function reset() {
	$('#date').val('');
	$('.section-row').addClass('hide');
	// the other 4
	$('.subject-row').addClass('hide');
	$('.teacher-row').addClass('hide');
	$('.timing-row').addClass('hide');

	$('#class_id_on_routine_creation option').each(function(i, v) {
		v.selected = false;
	});

	$('#class_id_on_routine_creation option').select2();
}

function onchangedate() {

	$('.section-row').addClass('hide');
	// the other 4
	$('.subject-row').addClass('hide');
	$('.teacher-row').addClass('hide');
	$('.timing-row').addClass('hide');

	$('#class_id_on_routine_creation option').select2();

	$('#class_id_on_routine_creation option').each(function(i, v) {
		v.selected = false;
	});

	$('#class_id_on_routine_creation option').select2();

	notice();
}

function classWiseSectionForRoutineCreate(classId) {
	if ( $('#date').val().trim() === "" ) return;
	$.ajax({
		url: "<?php echo route('section/checkboxlist/'); ?>"+classId,
		success: function(response){
		console.log('==='+response);
			$('#sectionDiv').html(response);
			$('.section-row').removeClass('hide');
			// the other 4
			$('.subject-row').addClass('hide');
			$('.teacher-row').addClass('hide');
			$('.timing-row').addClass('hide');
			notice();
		}
	});
}

function onchangeSection() 
{
// On change section.
// collect the section ids.

var classId = $('#class_id_on_routine_creation').val();
var values = [];
$('.section-input').each(function(i, v) {
	if ( v.checked ) {
		values.push(v.value);
	}
});

if ( values.length  >= 1 )
{
	// Now ajax.
	$.ajax({
		url: "<?php echo route('section/subjectlist/'); ?>",
		type: 'POST',
		data: {
			classId: classId,
			sections: values.join('-')
		},
		success: function(response){
		console.log('==='+response);

		$('.subject-row select').html(response);
		$('.subject-row').removeClass('hide');

		$('.teacher-row').addClass('hide');
		$('.timing-row').addClass('hide');
		notice();
		
			// $('#sectionDiv').html(response);
			// $('.section-row').removeClass('hide');
			// // the other 4
			// $('.subject-row').addClass('hide');
			// $('.teacher-row').addClass('hide');
			// $('.timing-row').addClass('hide');
		}
	});
} else {
	$('.subject-row').addClass('hide');
	$('.teacher-row').addClass('hide');
	$('.timing-row').addClass('hide');
}

}

function onchangeSubject() {

var val = $('.subject-row select').val();
if ( !val ) {
	$('.teacher-row').addClass('hide');
	$('.timing-row').addClass('hide');
	return;
}

var classId = $('#class_id_on_routine_creation').val();
var values = [];
$('.section-input').each(function(i, v) {
	if ( v.checked ) {
		values.push(v.value);
	}
});

// Now ajax.
$.ajax({
	url: "<?php echo route('section/teacherlist/'); ?>",
	type: 'POST',
	data: {
		classId: classId,
		sections: values.join('-'),
		subjectId: val
	},
	success: function(response){
	console.log('==='+response);

	$('.teacher-row select').html(response);
	$('.teacher-row').removeClass('hide');

	$('.timing-row').addClass('hide');
	notice();
	}
});
}

function onchangeTeacher() {

var val = $('.teacher-row select').val();
if ( !val ) {        
	$('.timing-row').addClass('hide');
	return;
}

var classId = $('#class_id_on_routine_creation').val();
var subjectId = $('.subject-row select').val();
var date = $('#date').val();

var values = [];
$('.section-input').each(function(i, v) {
	if ( v.checked ) {
		values.push(v.value);
	}
});

$.ajax({
	url: "<?php echo route('section/timelist/'); ?>",
	type: 'POST',
	data: {
		classId: classId,
		sections: values.join('-'),
		subjectId: subjectId,
		teacherId: val,
		date: date
	},
	success: function(response){
	console.log('==='+response);

		$('.timing-row select').html(response);
		$('.timing-row').removeClass('hide');
		notice();
	}
});


}

function onchangeTimeSlot()
{
var val =  $('.timing-row select').val();

if ( val == "-1" ) return;
if ( val == "0" ) {
	var $name =  $('.teacher-row select option:selected').html();
	var teacherId = $('.teacher-row select').val();
	var time = $('.timing-row select option:selected').data('val');
	var html =  $('.timing-row select option:selected').html();

	if ( html.search('Sections') != -1 ) {
		notice("This Time slot for selected Division/s combination is already booked.");
		return;
	}

//     $.ajax({
//     url: "<?php // echo route('section/timecheck/'); ?>",
//     type: 'POST',
//     data: {
//         time: time,
//         teacherId: teacherId
//     },
//     success: function(response){
//         console.log('==='+response);
//         $('.timing-row select').html(response);
//         $('.timing-row').removeClass('hide');
//         notice();
//     }
// });

	notice("Selected time slot is already booked for <strong>" + $name + "</strong> somewhere else on this Day.");
} else {
	notice();
}

}

function notice( e ) {
$noticeW = $('.section-notice');

if ( !e || e === "" )
{
	$noticeW.addClass("hide");
	return;
}

$noticeW.html(e);
$noticeW.removeClass('hide');
}


function classWiseSubjectForRoutineCreate(classId) {
	$.ajax({
		url: "<?php echo route('class_wise_subject/'); ?>"+classId,
		success: function(response){
			$('#subject_id_on_routine_creation').html(response);
		}
	});
}

function onclickContinue()
{
	$('#hidden_value').val(1);
	submit();
}

function _customAjaxSubmit(form, callBackFunction) {

	if(form.valid()) {	

		var action = '<?php echo route('routine/create'); ?>';		
		var data = new FormData(form[0]);
		console.log(data);
		$.ajax({
			type: "POST",
			url: action,
			processData: false,
			contentType: false,
			dataType: 'json',
			data: data,
			success: function(response)
			{
				if (response.status) {
					toastr.success(response.notification);
					if(form.attr('class') === 'ajaxDeleteForm'){
						$('#alert-modal').modal('toggle')
					}else{
						var hv = $('#hidden_value').val();
						$('#hidden_value').val(0);
						if ( hv != 1 ) {
							$('#right-modal').modal('hide');
							
						} else {



							$('.teacher-row select option').select2();

							$('.teacher-row select option').each(function(i, v) {
								v.selected = false;
							});

							$('.teacher-row select option').select2();

							$('.timing-row').addClass('hide');

							$('#subject_id_on_routine_creation option').select2();

							$('#subject_id_on_routine_creation option').each(function(i, v) {
								v.selected = false;
							});

							$('#subject_id_on_routine_creation option').select2();

							$('.teacher-row').addClass('hide');
						}


						// reset fields
						
					}
					callBackFunction();
				}else{
					toastr.error(response.notification);
				}
			}
		});
	}else {
		toastr.error('Please make sure to fill all the necessary fields');
	}
}

function onclickclose()
{
	reset();
}
</script>
