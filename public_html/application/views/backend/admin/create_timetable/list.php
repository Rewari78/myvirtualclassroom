<?php if(isset($class_id) && isset($from_date) && isset($to_date)): ?>
    <?php $dates =  displayDates($from_date, $to_date); ?>
    <?php $times = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id))->result_array(); ?>    
    <?php $timeIndex = array(); ?>    
    <?php
    
        //GEt teachers
        $SQL = "SELECT `t`.`id`, `u`.`id` AS `user_id`, `u`.`name` FROM `users` AS `u`
                INNER JOIN `teachers` AS `t`
                ON ( `u`.`id` = `t`.`user_id` )
                INNER JOIN `routines` AS `r`
                ON ( `r`.`teacher_id` = `t`.`id` )
                ";
        $result = $this->db->query($SQL)->result_array();
        $teachers = array();
        foreach ( $result as $row )
        {
            $teachers[$row['id']] = $row;
        }

        // Set sections
        $SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
        $result = $this->db->query($SQL, [$class_id])->result_array();
        $sections = array();
        foreach( $result as $row )
        {
            $sections[$row['id']] = $row;
        }
        // Set sections
        $SQL = "SELECT * FROM `common_subject`";
        $result = $this->db->query($SQL)->result_array();
        $subjects = array();
        foreach( $result as $row )
        {
            $subjects[$row['id']] = $row;
        }
    ?>
    <div class="list-table">
        <table class="table">
            <tr class="table-row">
                <td></td>
                <?php $i = 1; ?>
                <?php foreach( $times as $time ): ?>
                    <td>
                        <?php
                            $slot = prepareTimeSlot($time['start_time_hour'], $time['start_time_min']) .'-'. prepareTimeSlot($time['end_time_hour'], $time['end_time_min']);
                            $timeIndex[] = $slot;
                            if ( $time['type'] != "1" )
                            {
                                echo "Period-" . $i;
                                $i++;
                            } else {
                                echo $slot . ($time['type'] == "1" ? " <br />(Break)" : "");
                            }
                            
                        ?>
                    </td>
                <?php  endforeach; ?>
            </tr>
            <?php foreach ( $dates as $date ): ?>
                <tr>
                    <td><?php                     
                    $day = date('l', strtotime($date));
                    echo $day . "<br />";
                    echo $date; 
                    
                    ?></td>
                    <?php foreach( $timeIndex as $time ): ?>
                        <td>
                            <?php
                                $dbValues = array($date, $time);
                                $SQL = "SELECT * FROM `routines` 
                                    WHERE `date` = ? AND `htimeslot` = ?";

                                if ( !empty($section_ids) ) 
                                {
                                    $placeHolders = array();
                                    foreach ( $section_ids as $section )
                                    {
                                        $dbValues[] = $section;
                                        $placeHolders[] = '?';
                                    }

                                    $SQL .= " AND `section_id` IN (" . implode(",", $placeHolders) . ") ";
                                }
                                $routines = $this->db->query($SQL, $dbValues)->result_array();                                
                                foreach ( $routines as $routine )
                                {
                                    $teacherName = isset($teachers[$routine['teacher_id']]) ? $teachers[$routine['teacher_id']]['name'] : 'Unknown';
                                    $subjectName = isset($subjects[$routine['subject_id']]) ? $subjects[$routine['subject_id']]['name'] : 'Unknown';
                                    
                                    $avlSections = array();
                                    foreach ( explode(',', $routine['section_id'])  as $id )
                                    {
                                        $avlSections[] = isset($sections[$id]) ? $sections[$id]['name'] : 'NA';
                                    }
                                    
                                    $rid = $routine['id'];
                                    $avlSections = implode(',', $avlSections);                                    
                                    $output = "<strong>ID:</strong> $rid <br /><strong>Time:</strong> $time<br /><strong>Subject:</strong> $subjectName<br /><strong>Teacher:</strong>$teacherName<br /><strong>Section:</strong> $avlSections<br />";

                                    echo '<div class="cal-slot">';
                                    echo '<span class="close-icon" title="Remove" onclick="onclickclose(this, ' . $routine['id'] . ');">x</span>';
                                    echo $output;
                                    echo '</div>';
                                }
                            ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php else: ?>
    <?php include APPPATH.'views/backend/empty.php'; ?>
<?php endif; ?>

<?php
 function displayDates($date1, $date2, $format = 'm/d/Y' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {
       $dates[] = date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }

 function prepareTimeSlot( $startH, $startM ) 
 {
    $stHour = $startH;
    $stMin = $startM;
    if($stMin<10){
        $stMin = '0'.$stMin;
    }
    
    if($stHour >=12){
        if($stHour != 12)
        $stHour = $startH - 12;
        
        if($startM == 0){
            return $stHour.' PM'; 
        }else{
            return $stHour.':'.$stMin.' PM'; 
        }
        
    }
    else{
        if($startM == 0){
            return $startH. ' AM'; 
        }else{
            return $startH.':'.$stMin. ' AM'; 
        }
    
    }
 }


?>
<script type="text/javascript">
 function onclickclose(elm, id)
 {
    if (!window.confirm("Do you really want to delete?")) { 
        return;
    }

    $(elm).parent().remove();
      // Now ajax.
    $.ajax({
        url: "<?php echo route('section/deleteroutine/'); ?>",
        type: 'POST',
        data: {
            id: id
        },
        success: function(response){

        }
    });
 }
</script>

<style>
    .dropdown-toggle::after{
        display: none;
    }
</style>
