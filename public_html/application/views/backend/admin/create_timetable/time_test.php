<?php $time_slots = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id))->result_array(); ?>
                <?php foreach($time_slots as $slot): ?>
                    <option value="<?php echo $slot['id']; ?>">
                        
                        
                        <?php 
                            
                            $timeString = "";
                            $stHour = $slot['start_time_hour'];
                            $stMin = $slot['start_time_min'];
                            if($stMin<10){
                                $stMin = '0'.$stMin;
                            }
                            if($stHour >=12){
                                if($stHour != 12)
                                $stHour = $slot['start_time_hour'] - 12;
                                
                                if($slot['start_time_min'] == 0){
                                    $timeString = $stHour.' PM'; 
                                }else{
                                    $timeString = $stHour.':'.$stMin.' PM'; 
                                }
                                
                            }
                            else{
                                if($slot['start_time_min'] == 0){
                                    $timeString = $slot['start_time_hour']. ' AM'; 
                                }else{
                                    $timeString = $slot['start_time_hour'].':'.$stMin. ' AM'; 
                                }
                            
                            }
                            
                            
                            $edHour = $slot['end_time_hour'];
                            $edMin = $slot['end_time_min'];
                            if($edMin<10){
                                $edMin = '0'.$edMin;
                            }
                            if($edHour >=12){
                                if($edHour != 12)
                                $edHour = $slot['end_time_hour'] - 12;
                                
                                if($slot['end_time_min'] == 0){
                                    $timeString = $timeString .' - '. $edHour.' PM'; 
                                }else{
                                     $timeString = $timeString .' - '.  $edHour.':'.$edMin.' PM'; 
                                }
                               
                            }
                            else{
                                if($slot['end_time_min'] == 0){
                                     $timeString = $timeString .' - '.  $slot['end_time_hour']. ' AM'; 
                                }else{
                                     $timeString = $timeString .' - '.  $slot['end_time_hour'].':'.$edMin. ' AM'; 
                                }
                                
                            }
                
                        
                            echo $timeString;
                        ?>
                        
                        
                        </option>
                <?php endforeach; ?>