<?php
$school_id = school_id();
$slots = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id))->result_array();
if (count($slots) > 0): ?>
<table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%">
    <thead>
        <tr style="background-color: #313a46; color: #ababab;">
            <th>Slot ID</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Break Time</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($slots as $slot): ?>
            <tr>
                <td><?php echo $slot['id'];?></td>
                <td><?php 
                
                $stHour = $slot['start_time_hour'];
                $stMin = $slot['start_time_min'];
                if($stMin<10){
                    $stMin = '0'.$stMin;
                }
                
                if($stHour >=12){
                    if($stHour != 12)
                    $stHour = $slot['start_time_hour'] - 12;
                    
                    if($slot['start_time_min'] == 0){
                        echo $stHour.' PM'; 
                    }else{
                        echo $stHour.':'.$stMin.' PM'; 
                    }
                    
                }
                else{
                    if($slot['start_time_min'] == 0){
                        echo $slot['start_time_hour']. ' AM'; 
                    }else{
                        echo $slot['start_time_hour'].':'.$stMin. ' AM'; 
                    }
                
                }
                ?>
                </td>
                <td><?php 
                
                $edHour = $slot['end_time_hour'];
                $edMin = $slot['end_time_min'];
                if($edMin<10){
                    $edMin = '0'.$edMin;
                }
                
                
                if($edHour >=12){
                    if($edHour != 12)
                    $edHour = $slot['end_time_hour'] - 12;
                    
                    if($slot['end_time_min'] == 0){
                        echo $edHour.' PM'; 
                    }else{
                        echo $edHour.':'.$edMin.' PM'; 
                    }
                   
                }
                else{
                    if($slot['end_time_min'] == 0){
                        echo $slot['end_time_hour']. ' AM'; 
                    }else{
                        echo $slot['end_time_hour'].':'.$edMin. ' AM'; 
                    }
                    
                }
                ?>
                </td>
                 <td>
                    <?php
                    if($slot['type']==1){
                        echo "Yes";
                    }else{
                        echo "No";
                    }
                    ?>
                    
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="confirmModal('<?php echo route('prepare/break/'.$slot['id']); ?>', showAllClasses)"><?php echo 'Mark as Break'; ?></a>
                    &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="confirmModal('<?php echo route('prepare/unbreak/'.$slot['id']); ?>', showAllClasses)"><?php echo 'Remove Break'; ?></a>
                    &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="confirmModal('<?php echo route('prepare/delete/'.$slot['id']); ?>', showAllClasses)"><?php echo get_phrase('delete'); ?></a>
                    
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
    <?php include APPPATH.'views/backend/empty.php'; ?>
<?php endif; ?>
