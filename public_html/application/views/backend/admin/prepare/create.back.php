<form method="POST" class="d-block ajaxForm" action="<?php echo route('prepare/create'); ?>">
    <div class="form-row">
        <input type="hidden" name="school_id" value="<?php echo school_id(); ?>">
      
      
       <div class="form-group col-md-12">
        <label for="starting_hour" class="col-md-12 col-form-label">Online Class Will Start From</label>
       
            <select name="starting_hour" id = "starting_hour_on_routine_creation" class="form-control select2" data-toggle="select2"  required>
                <option value="">Select</option>
                <?php for($i = 7; $i <= 19; $i++){
                    if ($i < 12){
                        if ($i == 0){ ?>
                            <option value="<?php echo $i; ?>">12 AM</option>
                        <?php }else{ ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?> AM</option>
                        <?php } ?>
                    <?php }else{ ?>
                        <?php $j = $i - 12; ?>

                        <?php if ($j == 0){ ?>
                            <option value="<?php echo $i; ?>">12 PM</option>
                        <?php }else{ ?>
                            <option value="<?php echo $i; ?>"><?php echo $j; ?> PM</option>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </select>
           
           
       
    </div>
        
        <div class="form-group col-md-12">
        <label for="starting_minute" class="col-md-12 col-form-label">Starting Minute</label>
       
            <select name="starting_minute" id = "starting_minute_on_routine_creation" class="form-control select2" data-toggle="select2"  required>             
                <option value="0">00</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
            </select>
           
           
       
    </div>
    
    <div class="form-group col-md-12">
        <label for="periods" class="col-md-12 col-form-label">Maximum number of Periods</label>
        
            <select name="periods" id = "periods" class="form-control select2" data-toggle="select2"  required>
                <option value="">Select</option>
                <?php for($i = 3; $i <= 15; $i++){
                    ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>  
                    <?php } ?>
            </select>
       
    </div>    
    
    <div class="form-group col-md-12">
        <label for="duration" class="col-md-12 col-form-label">One period will be of</label>
        
            <select name="duration" id = "duration" class="form-control select2" data-toggle="select2"  required>
                <option value="">Select</option>
                <?php for($i = 5; $i <= 60; $i+=5){
                    ?>
                       <option value="<?php echo $i; ?>"><?php echo $i.' Mins'; ?></option>  
                    <?php } ?>
            </select>
       
    </div>
    
    
     <div class="form-group col-md-12">
        <label for="interval" class="col-md-12 col-form-label">Break between periods will be of</label>
        
            <select name="interval" id = "interval" class="form-control select2" data-toggle="select2"  required>
                
                
                <option value="">Select</option>
                 <option value="2"><?php echo '2 Mins'; ?></option>  
                <?php for($i = 5; $i <= 60; $i+=5){
                    ?>
                       <option value="<?php echo $i; ?>"><?php echo $i.' Mins'; ?></option>  
                    <?php } ?>
            </select>
       
    </div>
      

 

        <div class="form-group  col-md-12">
            <button class="btn btn-block btn-primary" type="submit"><?php echo 'SAVE'; ?></button>
        </div>
    </div>
</form>

<script>
$(document).ready(function () {

    initSelect2([
    '#starting_hour_on_routine_creation',
    '#starting_minute_on_routine_creation',
    '#ending_hour_on_routine_creation',
    '#ending_minute_on_routine_creation',
    '#duration',
    '#interval',
    '#periods']);
});
$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
    var form = $(this);
    ajaxSubmit(e, form, showAllClasses);
});
</script>
