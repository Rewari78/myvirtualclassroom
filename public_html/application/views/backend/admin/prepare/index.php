<?php 
$school_id = school_id();
$slots = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id))->result_array();
?><!--title-->
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title"> 
            
            <i class="mdi mdi-book-open-page-variant title_icon"></i> <?php echo 'Prepare Time Slot'; ?>
            
            <?php if ( empty($slots) ): ?>
                <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle" onclick="rightModal('<?php echo site_url('modal/popup/prepare/create'); ?>', '<?php echo 'Add Time Slot'; ?>')"> <i class="mdi mdi-plus"></i> <?php echo 'Add Time Slot'; ?></button> 
            <?php else: ?>             
                <button type="button" class="btn btn-outline-danger btn-rounded alignToTitle " style="margin-right:10px" onclick="confirmModal('<?php echo route('prepare/deleteall/'); ?>', showAllClasses)"> <?php echo 'Delete all Time Slot'; ?></button>
            <?php endif; ?>
              
             
               
               
            
            
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body class_content">
                <?php include 'list.php'; ?>
            </div>
        </div>
    </div>
</div>

<script>
function myFunction() {
    
    var url = '<?php echo route('prepare/slotlist'); ?>';

        $.ajax({
            type : 'GET',
            url: url,
            success : function(response) {
               console.log(response);
            }
        });

 
}

    var showAllClasses = function () {
        var url = '<?php echo route('prepare/list'); ?>';

        $.ajax({
            type : 'GET',
            url: url,
            success : function(response) {
                $('.class_content').html(response);
                initDataTable('basic-datatable');
                window.location.reload();
            }
        });
    }
</script>
