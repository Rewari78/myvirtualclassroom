<form method="POST" class="d-block ajaxForm" action="<?php echo route('assign_subject/create'); ?>">
  <div class="form-row">

    <input type="hidden" name="school_id" value="<?php echo school_id(); ?>">
    <input type="hidden" name="session" value="<?php echo active_session();?>">

    <div class="form-group col-md-12">
      <label for="class_id_on_create"><?php echo get_phrase('class'); ?></label>
      <select name="class_id" id="class_id_on_create" class="form-control select2" data-toggle="select2" onchange="classWiseSection(this.value)" required>
        <option value=""><?php echo get_phrase('select_a_class'); ?></option>
        <?php
        $classes = $this->db->get_where('classes', array('school_id' => school_id()))->result_array();
        foreach($classes as $class){
          ?>
          <option value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>
        <?php } ?>
      </select>
      <small id="class_help" class="form-text text-muted"><?php echo get_phrase('select_a_class'); ?></small>
    </div>
    
    
     <div class="form-group col-md-12">
      <label for="class_id_on_create"><?php echo get_phrase('section'); ?></label>
      <select name="section" id="section_id" class="form-control select2" data-toggle = "select2"  required>
                        <option value=""><?php echo get_phrase('select_a_section'); ?></option>
                    </select>
     
    </div>
    
    
    <div class="form-group col-md-12">
      <label for="sub_id_on_create"><?php echo "Select Subject"; ?></label>
      <select name="sub_id" id="sub_id_on_create" class="form-control select2" data-toggle="select2" required>
        <option value=""><?php echo "Select Subject"; ?></option>
        <?php
            $subjects = $this->db->get_where('common_subject', array())->result_array();
                foreach($subjects as $subject){
          ?>
          <option value="<?php echo $subject['id']; ?>"><?php echo $subject['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    

    <div class="form-group  col-md-12">
      <button class="btn btn-block btn-primary" type="submit"><?php echo 'SAVE'; ?></button>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
  initSelect2(['#class_id_on_create']);
  initSelect2(['#sub_id_on_create']);
   initSelect2(['#section_id']);
});

function classWiseSection(classId) {
        $.ajax({
            url: "<?php echo route('section/list/'); ?>"+classId,
            success: function(response){
                console.log(response);
                var count = (response.match(/option/g) || []).length;
                //we are adding ALL when there are more then one section
                if(count>2){
                    var allSection = '<option value="ALL">ALL</option>';
                     $('#section_id').html(allSection+response);
                }else{
                     $('#section_id').html(response);
                }
              
               
            }
        });
    }


$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
  var form = $(this);
  ajaxSubmit(e, form, showAllSubjects);
});
</script>
