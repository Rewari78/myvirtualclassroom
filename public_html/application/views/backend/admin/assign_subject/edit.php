<?php $school_id = school_id(); ?>
<?php $subjects = $this->db->get_where('subjects', array('id' => $param1))->result_array(); ?>
<?php foreach($subjects as $subject){ ?>
<form method="POST" class="d-block ajaxForm" action="<?php echo route('assign_subject/update/'.$param1); ?>">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="class"><?php echo get_phrase('class'); ?></label>
            <select name="class_id" id="class_id_on_create" class="form-control select2" data-toggle="select2" onchange="classWiseSection(this.value)" required>
                <option value=""><?php echo get_phrase('select_a_class'); ?></option>
                    <?php
                        $classes = $this->db->get_where('classes', array('school_id' => $school_id))->result_array();
                        foreach($classes as $class){
                    ?>
                        <option value="<?php echo $class['id']; ?>" <?php if($class['id'] == $subject['class_id'])
                        { echo 'selected'; 
                        $class_id = $class['id'];
                        } 
                        ?>>
                            <?php echo $class['name']; ?></option>
                    <?php } ?>
            </select>
            <small id="class_help" class="form-text text-muted"><?php echo get_phrase('select_a_class'); ?></small>
        </div>
        
        
        
        <div class="form-group col-md-12">
            <label for="class">Section</label>
            <select name="section" id="section_id" class="form-control select2" data-toggle="select2" required>
                <option value=""><?php echo get_phrase('select_a_section'); ?></option>
                    <?php
                        $sections = $this->db->get_where('sections', array('class_id' => $class_id))->result_array();
                        foreach($sections as $section){
                    ?>
                        <option value="<?php echo $section['id']; ?>" <?php if($section['id'] == $subject['section_id']){ echo 'selected'; } ?>><?php echo $section['name']; ?></option>
                    <?php } ?>
            </select>
            <small id="class_help" class="form-text text-muted"><?php echo get_phrase('select_a_class'); ?></small>
        </div>
        
        
        <div class="form-group col-md-12">
      <label for="sub_id_on_create"><?php echo "Select Subject"; ?></label>
      <select name="sub_id" id="sub_id_on_create" class="form-control select2" data-toggle="select2" required>
        <option value=""><?php echo "Select Subject"; ?></option>
        <?php
            $subjects = $this->db->get_where('common_subject', array())->result_array();
                foreach($subjects as $subject){
          ?>
          <option value="<?php echo $subject['id']; ?>"><?php echo $subject['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    

        <div class="form-group col-md-12">
            <label for="name"><?php echo get_phrase('subject_name'); ?></label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $subject['name']; ?>" required>
            <small id="name_help" class="form-text text-muted"><?php echo get_phrase('provide_subject_name'); ?></small>
        </div>

        <div class="form-group  col-md-12">
            <button class="btn btn-block btn-primary" type="submit">UPDATE</button>
        </div>
    </div>
</form>
<?php } ?>

<script>
$(".ajaxForm").validate({}); // Jquery form validation initialization
$(".ajaxForm").submit(function(e) {
  var form = $(this);
  ajaxSubmit(e, form, showAllSubjects);
});

function classWiseSection(classId) {
        $.ajax({
            url: "<?php echo route('section/list/'); ?>"+classId,
            success: function(response){
                console.log(response);
                var count = (response.match(/option/g) || []).length;
                //we are adding ALL when there are more then one section
                if(count>2){
                    var allSection = '<option value="ALL">ALL</option>';
                     $('#section_id').html(allSection+response);
                }else{
                     $('#section_id').html(response);
                }
              
               
            }
        });
    }
    
$(document).ready(function() {
  initSelect2(['#class_id_on_create']);
  initSelect2(['#sub_id_on_create']);
   initSelect2(['#section_id']);
});
</script>
