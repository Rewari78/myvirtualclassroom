<?php
$sections = $this->db->order_by('section_id', 'ASC')->get_where('routines', array('class_id' => $class_id, 'school_id' => $school_id))->result_array();
$output = [];
foreach ( $sections as $section ) {
  $output[$section['section_id']] = $section['section_id'];
}
// Set sections
$SQL = "SELECT * FROM `sections` WHERE `class_id` = ?";
$result = $this->db->query($SQL, [$class_id])->result_array();
$avlSec = array();
foreach( $result as $row )
{
    $avlSec[$row['id']] = $row;
}

$sections = $output;
if (count($sections) > 0): ?>


  <?php foreach ($sections as $section): ?>   
  <div class="section-select">
    <div class="section-check-wrapper">
      <input type="checkbox" id="sections" name="sections[]" value="<?php echo $section; ?>" class="form-check-input combo-section-input">
      <div class="section-check"></div>
    </div>    
  <?php 
    $s = explode(",", $section);
    $ss = array();
    foreach ( $s as $item )
    {
      $ss[] = isset($avlSec[$item]) ? $avlSec[$item]['name'] : 'Unknown';
    }
    echo implode(",", $ss);
    ?>      
  </div>
  
  <?php endforeach; ?>
<?php else: ?>
  <?php echo get_phrase('no_section_found'); ?>
<?php endif; ?>
