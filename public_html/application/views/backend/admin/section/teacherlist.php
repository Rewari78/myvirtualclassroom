<?php
/**
 * @var CI_DB_query_builder $db
 */
$db = $this->db;
$placeHolders = array_fill(0, count($section_ids), '?');
$dbValues = array($class_id);
foreach ( $section_ids as $sec )
{
  $dbValues[] = $sec;
}
$dbValues[] = $school_id;
$dbValues[] = $subject_id;
$SUBSQL = "SELECT `s`.`teacher_id` FROM `subjects` AS `s`
  WHERE `s`.`class_id` = ? AND 
  `s`.`section_id` IN (" . implode(",", $placeHolders) . ") 
  AND `s`.`school_id` = ? 
  AND `s`.`common_subject_id` = ?
  ";
$SQL = "SELECT 
          `t`.`id`, `t`.`user_id`, `u`.`name`
        FROM
          `teachers` AS `t`
        INNER JOIN `users` AS `u`
        ON (`u`.`id` = `t`.`user_id`)
        WHERE `t`.`id` IN ({$SUBSQL})
        ";
        
$subjects = $db->query($SQL, $dbValues)->result_array();
if (count($subjects) > 0):?>
   <option value="">Select A Teacher</option>
  <?php foreach ($subjects as $subject): ?>
    <option value="<?php echo $subject['id']; ?>"><?php echo $subject['name']; ?></option>
  <?php endforeach; ?>
<?php else: ?>
  <option value=""><?php echo "No teacher found" ?></option>
<?php endif; ?>
