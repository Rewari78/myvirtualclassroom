<?php
$dbValues = array(
  'class_id' => $class_id,
  'school_id' => $school_id,
  'section_id' => json_decode($section_ids, true),
  'from_date' => $from_date,
  'to_date' =>  $to_date,
  'overwrite' => $overwrite
);

$toDates = displayDates($to_date, 7);
$fromDates = displayDates($from_date, 7);

if ( $overwrite === 'true' )
{
  $placeholders = array_fill(0, count($toDates), '?');
  $sectionHolder = array_fill(0, count($dbValues['section_id']), '?');
  $SQL = "SELECT * FROM `routines` WHERE `class_id` = ? AND `school_id` = ? AND `date` IN (" . implode(",", $placeholders) . ") AND `section_id` IN (" . implode(",", $sectionHolder) . ")";
  $DSQL = "DELETE FROM `routines` WHERE `class_id` = ? AND `school_id` = ? AND `date` IN (" . implode(",", $placeholders) . ") AND `section_id` IN (" . implode(",", $sectionHolder) . ")";
  $bind = array(
    $class_id,
    $school_id,
  );

  foreach ( $toDates as $date ) 
  {
    $bind[] = $date;
  }

  foreach ( $dbValues['section_id'] as $s ) 
  {
    $bind[] = $s;
  }

  $result = $this->db->query($SQL, $bind)->result_array();
  foreach ( $result as $row )
  {
    $this->db->query("DELETE FROM `busy_sections` WHERE `routine_id` = ?", $row['id']);
  }

  $this->db->query($DSQL, $bind);  
}

foreach ( $fromDates as $key => $date ) 
{
  $copyDate = $toDates[$key];
  $placeholders = array_fill(0, count($dbValues['section_id']), '?');
  $SQL = "SELECT * FROM `routines` WHERE `class_id` = ? AND `section_id` IN (" . implode(",", $placeholders) . ") AND `school_id` = ? AND `date` = ?";
  $newD =  array(
    $dbValues['class_id']
  );
  foreach ( $dbValues['section_id'] as $value ) {
    $newD[] = $value;
  }
  $newD[] = $dbValues['school_id'];
  $newD[] = $date;  
  $result = $this->db->query($SQL, $newD)->result_array();
  
  foreach ( $result as $row )
  {
    $SQL = "SELECT * FROM `routines` WHERE `class_id` = ? AND `section_id` = ? AND `school_id` = ? AND `date` = ?";    

    // else insert the date.

    unset($row['id']);
    $row['date'] = $copyDate;

    $this->db->insert('routines', $row);
    $insertId = $this->db->insert_id();
    $newData = array();
    $sections = explode(",", $row['section_id']);
    
		// Nowe we h ave to fill the sections.about-image
		foreach ( $sections as $section )
		{
			$newData['section_id'] = $section;
			$newData['date'] = $copyDate;
			$newData['htimeslot'] = $row['htimeslot'];
			$newData['routine_id'] = $insertId;
			$this->db->insert('busy_sections', $newData);
		}
  }
  
}

function displayDates($date1, $upto, $format = 'm/d/Y' ) {
  $dates = array();
  $current = strtotime($date1);  
  $stepVal = '+1 day';
  $i = 0;
  while( $i < $upto ) {
     $dates[] = date($format, $current);
     $current = strtotime($stepVal, $current);
     $i++;
  }
  return $dates;
}

header("content-type: application/json");
echo json_encode(array("msg" => "success"));
exit;