<?php
$sections = $this->db->get_where('sections', array('class_id' => $class_id))->result_array();
if (count($sections) > 0):
  foreach ($sections as $section): ?>  
  <div class="section-select">
    <div class="section-check-wrapper">
      <input type="checkbox" id="sections" name="sections[]" value="<?php echo $section['id']; ?>" onchange="onchangeSection();" class="form-check-input section-input">
      <div class="section-check"></div>
    </div>  
    <?php echo $section['name']; ?>
  </div>
  <?php endforeach; ?>
<?php else: ?>
  <?php echo get_phrase('no_section_found'); ?>
<?php endif; ?>
