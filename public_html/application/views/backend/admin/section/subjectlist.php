<?php
/**
 * @var CI_DB_query_builder $db
 */
$db = $this->db;
$placeHolders = array_fill(0, count($section_ids), '?');
$dbValues = array($class_id);
foreach ( $section_ids as $sec )
{
  $dbValues[] = $sec;
}
$dbValues[] = $school_id;
$SQL = "SELECT * FROM `subjects` WHERE `class_id` = ? AND `section_id` IN (" . implode(",", $placeHolders) . ") AND `school_id` = ? GROUP BY `common_subject_id`";
$subjects = $db->query($SQL, $dbValues)->result_array();
if (count($subjects) > 0):
  ?> <option value="">Select A Subject</option> <?php
  foreach ($subjects as $subject): ?>
    <option value="<?php echo $subject['common_subject_id']; ?>"><?php echo $subject['name']; ?></option>
  <?php endforeach; ?>
<?php else: ?>
  <option value=""><?php echo "No subject found" ?></option>
<?php endif; ?>
