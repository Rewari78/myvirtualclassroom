<?php

$dbValues[] = $teacher_id;
$dbValues[] = $school_id;
$dbValues[] = $date_c;

// Get the time list for the teachers 
// So that we can say when can teacher not be available.
$SQL = "SELECT * FROM `routines` WHERE `teacher_id` = ? AND `school_id` = ? AND `date` = ?";
$routines = $this->db->query($SQL, $dbValues)->result_array();

?>
<?php $time_slots = $this->db->order_by("id", "asc")->get_where('time_slots', array('school_id' => $school_id, 'type' => 0))->result_array(); ?>
<option value="-1"><?php echo "Select A Time"; ?></option>
<?php foreach($time_slots as $slot): ?>
  <?php $isSlotAvailable = _isSlotAvailable($slot, $routines); ?>
  <?php $time = prepareTimeSlot($slot['start_time_hour'], $slot['start_time_min']) .'-'. prepareTimeSlot($slot['end_time_hour'], $slot['end_time_min']); ?>
    <option value="<?php echo $isSlotAvailable ? $slot['id'] : '0'; ?>" data-val="<?php echo $time; ?>">
        <?php 
            
            $timeString = "";
            $stHour = $slot['start_time_hour'];
            $stMin = $slot['start_time_min'];
            if($stMin<10){
                $stMin = '0'.$stMin;
            }
            if($stHour >=12){
                if($stHour != 12)
                $stHour = $slot['start_time_hour'] - 12;
                
                if($slot['start_time_min'] == 0){
                    $timeString = $stHour.' PM'; 
                }else{
                    $timeString = $stHour.':'.$stMin.' PM'; 
                }
                
            }
            else{
                if($slot['start_time_min'] == 0){
                    $timeString = $slot['start_time_hour']. ' AM'; 
                }else{
                    $timeString = $slot['start_time_hour'].':'.$stMin. ' AM'; 
                }
            
            }
            
            
            $edHour = $slot['end_time_hour'];
            $edMin = $slot['end_time_min'];
            if($edMin<10){
                $edMin = '0'.$edMin;
            }
            if($edHour >=12){
                if($edHour != 12)
                $edHour = $slot['end_time_hour'] - 12;
                
                if($slot['end_time_min'] == 0){
                    $timeString = $timeString .' - '. $edHour.' PM'; 
                }else{
                      $timeString = $timeString .' - '.  $edHour.':'.$edMin.' PM'; 
                }
                
            }
            else{
                if($slot['end_time_min'] == 0){
                      $timeString = $timeString .' - '.  $slot['end_time_hour']. ' AM'; 
                }else{
                      $timeString = $timeString .' - '.  $slot['end_time_hour'].':'.$edMin. ' AM'; 
                }
                
            }

        
            echo $timeString;

            echo $isSlotAvailable ? ' (Available)' : '';
        ?>
        
        
        </option>
<?php endforeach; ?>
<?php
function _isSlotAvailable( $slot, $routines )
{
  $sstartH = $slot['start_time_hour'];
  $sstartM = $slot['start_time_min'];
  $sendH = $slot['end_time_hour'];
  $sendM = $slot['end_time_min'];

  

  // if ( $ )
  // var_dump($routines, $slot);exit;

  foreach ( $routines as $routine )
  {
    $rstartH = $routine['starting_hour'];
    $rendH = $routine['ending_hour'];
    $rstartM = $routine['starting_minute'];
    $rendM = $routine['ending_minute'];

    if ( 
      $rstartH == $sstartH &&
      $rstartM == $sstartM &&
      $rendH == $sendH &&
      $rendM == $sendM
     ) {
       return false;
     }

  }
  
  return true;
}


function prepareTimeSlot( $startH, $startM ) 
{
   $stHour = $startH;
   $stMin = $startM;
   if($stMin<10){
       $stMin = '0'.$stMin;
   }
   
   if($stHour >=12){
       if($stHour != 12)
       $stHour = $startH - 12;
       
       if($startM == 0){
           return $stHour.' PM'; 
       }else{
           return $stHour.':'.$stMin.' PM'; 
       }
       
   }
   else{
       if($startM == 0){
           return $startH. ' AM'; 
       }else{
           return $startH.':'.$stMin. ' AM'; 
       }
   
   }
}