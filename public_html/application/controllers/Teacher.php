<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
*  @author   : Creativeitem
*  date      : November, 2019
*  Ekattor School Management System With Addons
*  http://codecanyon.net/user/Creativeitem
*  http://support.creativeitem.com
*/

class Teacher extends CI_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->database();
		$this->load->library('session');

		/*LOADING ALL THE MODELS HERE*/
		$this->load->model('Crud_model',     'crud_model');
		$this->load->model('User_model',     'user_model');
		$this->load->model('Settings_model', 'settings_model');
		$this->load->model('Payment_model',  'payment_model');
		$this->load->model('Email_model',    'email_model');
		$this->load->model('Addon_model',    'addon_model');
		$this->load->model('Frontend_model', 'frontend_model');

		/*cache control*/
		$this->output->set_header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");

		/*SET DEFAULT TIMEZONE*/
		timezone();
		
		if($this->session->userdata('teacher_login') != 1){
			redirect(site_url('login'), 'refresh');
		}
	}
	//dashboard
	public function index(){
		redirect(route('dashboard'), 'refresh');
	}

	public function dashboard( $param = '' ){

		if ( $param == 'savecomment' ) {
			$id = isset($_POST['id']) ? $_POST['id'] : null;
			$comment =  isset($_POST['comment']) ? $_POST['comment'] : null;
			if ( !$id || !$comment ) {
				exit;
			}			
			$this->crud_model->saveComment($id, $comment);
			exit;
		}

		$page_data['page_title'] = 'Dashboard';
		$page_data['folder_name'] = 'dashboard';
		$this->load->view('backend/index', $page_data);
	}

	public function download_a( $param1 = '', $param2 = '', $param3 = '', $param4 = '', $param5 = '', $param6 = '', $param7 = '' )
	{
		$param1 = trim(rawurldecode($param1));
		$param2 = trim(rawurldecode($param2));
		$param3 = trim(rawurldecode($param3));
		$param4 = trim(rawurldecode($param4));
		$param5 = trim(rawurldecode($param5));
		$param6 = trim(rawurldecode($param6));
		$param7 = trim(rawurldecode($param7));

		// first select all student from sections
		$SQL = "SELECT `u`.`name`, `e`.`student_id`, `e`.`section_id` FROM `enrols` AS `e`
		INNER JOIN `students` as `s` ON ( `e`.`student_id` = `s`.`id`)
		INNER JOIN `users` AS `u` ON  ( `s`.`user_id` = `u`.`id` )
		WHERE `e`.`section_id` IN (?, ?)";
		$sections = explode('-', $param3);
		if ( count($sections) <= 1 ) {
			$sections[] = 0;
		}		
		$result = $this->db->query($SQL, $sections)->result_array();

		// attended students
		$SQL = "SELECT * FROM `attendance` WHERE `routine_id` = ?";
		$result2 = $this->db->query($SQL, [$param4])->result_array();

		$aIds =[];
		foreach ( $result2 as $r ) 
		{
			$aIds[] = $r['student_id'];
		}

		$param5 .= ' - ' . str_replace('-', '/', $param6);
		$date = str_replace('-', '/', $param1);		
		$date = date('d-m-Y', strtotime($date));

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="Attendance (' . $param2 . ' - ' . $date . ' - ' .  $param5 . ').csv"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		echo 'Date,, ' . $date . "\r\n";
		echo 'Time,, ' . $param7 . "\r\n";
		echo 'Subject,, ' . $param2 . "\r\n";
		echo 'STD,, ' . $param5 . "\r\n";
		echo 'Name, Status, Section' . "\r\n";

		foreach ( $result as $r )
		{
			$isP = in_array($r['student_id'], $aIds) ? "Present" : "Absent";
			$section = $this->db->query("SELECT * FROM sections WHERE `id` = ?", [$r['section_id']])->result_array();
			$section = count($section) >= 1 ? $section[0]['name'] : 'Unknown';
			echo $r['name'] . ', ' . $isP . ", {$section}\r\n";
		}

		// var_dump($result);
		// // $result =
		// var_dump($param1, $param2, $param3, $param4);
	}

	//START STUDENT ADN ADMISSION section
	public function student($param1 = '', $param2 = '', $param3 = '', $param4 = '', $param5 = ''){

		if($param1 == 'create'){
			//form view
			if($param2 == 'bulk'){
				$page_data['aria_expand'] = 'bulk';
				$page_data['working_page'] = 'create';
				$page_data['folder_name'] = 'student';
				$page_data['page_title'] = 'add_student';
				$this->load->view('backend/index', $page_data);
			}elseif($param2 == 'excel'){
				$page_data['aria_expand'] = 'excel';
				$page_data['working_page'] = 'create';
				$page_data['folder_name'] = 'student';
				$page_data['page_title'] = 'add_student';
				$this->load->view('backend/index', $page_data);
			}else{
				$page_data['aria_expand'] = 'single';
				$page_data['working_page'] = 'create';
				$page_data['folder_name'] = 'student';
				$page_data['page_title'] = 'add_student';
				$this->load->view('backend/index', $page_data);
			}
		}

		//create to database
		if($param1 == 'create_single_student'){
			$response = $this->user_model->single_student_create();
			echo $response;
		}

		if($param1 == 'create_bulk_student'){
			$response = $this->user_model->bulk_student_create();
			echo $response;
		}

		if($param1 == 'create_excel'){
			$response = $this->user_model->excel_create();
			echo $response;
		}

		// form view
		if($param1 == 'edit'){
			$page_data['student_id'] = $param2;
			$page_data['working_page'] = 'edit';
			$page_data['folder_name'] = 'student';
			$page_data['page_title'] = 'update_student_information';
			$this->load->view('backend/index', $page_data);
		}

		//updated to database
		if($param1 == 'updated'){
			$response = $this->user_model->student_update($param2, $param3);
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->user_model->delete_student($param2, $param3);
			echo $response;
		}

		if($param1 == 'filter'){
			$page_data['class_id'] = $param2;
			$page_data['section_id'] = $param3;
			$this->load->view('backend/teacher/student/list', $page_data);
		}

		if(empty($param1)){
			$page_data['working_page'] = 'filter';
			$page_data['folder_name'] = 'student';
			$page_data['page_title'] = 'student_list';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END STUDENT ADN ADMISSION section

	//START TEACHER section
	public function teacher($param1 = '', $param2 = '', $param3 = ''){


		if($param1 == 'create'){
			$response = $this->user_model->create_teacher();
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->user_model->update_teacher($param2);
			echo $response;
		}

		if($param1 == 'delete'){
			$teacher_id = $this->db->get_where('teachers', array('user_id' => $param2))->row('id');
			$response = $this->user_model->delete_teacher($param2, $teacher_id);
			echo $response;
		}

		if ($param1 == 'list') {
			$this->load->view('backend/teacher/teacher/list');
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'teacher';
			$page_data['page_title'] = 'techers';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END TEACHER section

	//START CLASS secion
	public function class($param1 = '', $param2 = '', $param3 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->class_create();
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->class_delete($param2);
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->crud_model->class_update($param2);
			echo $response;
		}

		if($param1 == 'section'){
			$response = $this->crud_model->section_update($param2);
			echo $response;
		}

		// show data from database
		if ($param1 == 'list') {
			$this->load->view('backend/teacher/class/list');
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'class';
			$page_data['page_title'] = 'class';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END CLASS section

	//	SECTION STARTED
	public function section($action = "", $id = "") {

		// PROVIDE A LIST OF SECTION ACCORDING TO CLASS ID
		if ($action == 'list') {
			$page_data['class_id'] = $id;
			$this->load->view('backend/teacher/section/list', $page_data);
		}

		if ( $action == 'sectioncombo' )
		{
			$page_data['school_id'] = $this->session->userdata['school_id'];
			$page_data['class_id'] = isset($_POST['classId']) ? $_POST['classId'] : '';			
			$this->load->view('backend/admin/section/seccombo', $page_data);
		}

		if ( $action == 'filter' )
		{
			$page_data['school_id'] = $this->session->userdata['school_id'];
			$page_data['class_id'] = isset($_POST['classId']) ? $_POST['classId'] : '';			
			$page_data['section_ids'] = isset($_POST['sectionIds']) ? json_decode($_POST['sectionIds'], true) : array();			
			$page_data['page_no'] = isset($_POST['pageNo']) ? $_POST['pageNo'] : '';			

			$this->load->view('backend/teacher/my_records/list', $page_data);
		}

		if ( $action == 'upload' )
		{
			$page_data['routine_id'] = isset($_POST['id']) ? $_POST['id'] : '';	
			$this->load->view('backend/teacher/section/upload_form', $page_data);
		}


		if ( $action == 'upload_file' )
		{
			$fileName = $_FILES['file']['name'];
			$fileSize = $_FILES['file']['size'];
			$file = $_FILES['file']['tmp_name'];
			$fileDetails = pathinfo($fileName);
			$ext = $fileDetails['extension'];
			$newName = md5(time()) . '.'  . $ext;
			$path = FCPATH . 'uploads/teacher_files/' . $newName;

			if ( move_uploaded_file($file, $path) ) {

				// Get the files info
				$id = isset($_POST['id']) ? $_POST['id'] : '';
				$result = $this->db->query("SELECT * FROM `routines` WHERE `id` = ? ", [$id])->result_array();

				$routine = $result[0];
				$t_files = json_decode($routine['t_files'], true);
				$t_files = is_array($t_files) ? $t_files : array();
				$filesJ = array();
				if ( empty($t_files) ) {
					$filesJ[] = array( 'name' => $fileName, 'file' => $newName );
				} else {					
					$filesJ = $t_files;
					$filesJ[] = array( 'name' => $fileName, 'file' => $newName );
				}

				$t_files = json_encode($filesJ);
				$this->db->query("UPDATE `routines` SET `t_files` = ? WHERE `id` = ? ", [$t_files, $id]);

				$index = count($filesJ) - 1;
				header('content-type: application/json');
				echo json_encode(array(
					'success' => true,
					'message' => '<a href="' . site_url('uploads/teacher_files/' . $newName) . '" target="_blank" style="text-decoration: none;">
					<strong>' . $fileName . '</strong>
					</a> <a href="javascript:void(0);" onclick="removeFile(' . $routine['id'] . ', ' . $index . ', this)" style="float: right;">Remove</a>'
				));

			} else {

				header('content-type: application/json');
				echo json_encode(array(
					'success' => false,
					'message' => 'Please file not moved please check for the permission.'
				));

			}
		}


		if ( $action == 'remove_file' )
		{
			$id = isset($_POST['id']) ? $_POST['id'] : '';	
			$index = isset($_POST['index']) ? $_POST['index'] : '';

			$result = $this->db->query("SELECT * FROM `routines` WHERE `id` = ? ", [$id])->result_array();

			$routine = $result[0];
			$t_files = json_decode($routine['t_files'], true);
			$t_files = is_array($t_files) ? $t_files : array();

			if ( isset($t_files[$index]) ) {
				$file = $t_files[$index];

				$path = FCPATH . 'uploads/teacher_files/' . $file['file'];
				@unlink($path);

				unset($t_files[$index]);
				$t_files = json_encode($t_files);
				$this->db->query("UPDATE `routines` SET `t_files` = ? WHERE `id` = ? ", [$t_files, $id]);

			}

			// So now we need to put the update.
			header('content-type: application/json');
			echo json_encode(array(
				'success' => true,
				'message' => '<input type="file" class="file-upload-section" onchange="onchangeFile(this, ' . $id . ')">
				<p class="hide">Uploading...</p>'
			));

			

			//remove te 
		}

		if ( $action == 'start' ) {
			
			$id = isset($_POST['id']) ? $_POST['id'] : '';
			$time = isset($_POST['start']) ? $_POST['start'] : '';
			$time = $time / 1000;
			$ttt = time();

			$arr = array(
				'success' => true
			);
			
			if ( $time - $ttt > 10 * 60 ) {
				$arr = array(
					'success' => false
				);
				header("content-type: application/json");
				echo json_encode($arr);
				exit;
			}

			header("content-type: application/json");
			echo json_encode($arr);
			exit;
			
		}

		if ( $action == 'deletemeeting' ) {
			
			$id = isset($_POST['id']) ? $_POST['id'] : '';
            $this->db->query("DELETE FROM `meetings` WHERE `routine_id` = ? ", [$id]);
            $arr = array(
                'success' => true
            );
            header("content-type: application/json");
            echo json_encode($arr);
            exit;
        }


		if ( $action == 'create' ) {
			
			$id = isset($_POST['id']) ? $_POST['id'] : '';
			$time = isset($_POST['start']) ? $_POST['start'] : '';
			$time = $time / 1000;
			$ttt = time();
			$subject = isset($_POST['subject']) ? $_POST['subject'] : '';
			$teacher = isset($_POST['teacher']) ? $_POST['teacher'] : '';
			$duration = isset($_POST['duration']) ? $_POST['duration'] : '';
			$msg = isset($_POST['msg']) ? $_POST['msg'] : '';			
			
			
			$url = 'http://newapi.myvirtualclassroom.in/createMeeting.php';
			
			$schoolId = $this->session->userdata['school_id'];
			$schoolCode = $this->db->get_where('schools', array('id' => $schoolId))->row('school_code');
			
			

			$result = $this->db->query("SELECT * FROM `routines` WHERE `id` = ? ", [$id])->result_array();
			$routine = $result[0];
			
			
            
                if($schoolCode == 'ak' && $routine['class_id'] == '2'){
                       $serverURL = 'https://vc02.myvirtualclassroom.in';
                    }else if($schoolCode == 'ak' && $routine['class_id'] == '3'){
                       $serverURL = 'https://vc02.myvirtualclassroom.in';
                    }else{
                        $serverURL = 'https://vc01.myvirtualclassroom.in';
                     }
				   
            
			
			$t_files = json_decode($routine['t_files'], true);
			$t_files = is_array($t_files) ? $t_files : array();

			// build xml.
			$modules = '';
			if ( !empty($t_files) )
			{
				$modules = '<modules>
				<module name="presentation">';
				$i=1;
				foreach( $t_files as $file ) {
				    //$fileName  = preg_replace('/[^a-zA-Z0-9_ -]/s','',$file['name']);
				    $fileName  = 'File'.$i;
					$modules .= '<document url="' . site_url('uploads/teacher_files/' . $file['file']) . '" filename="' . $fileName .'"/>';
					$i++;
				}				
				$modules .= '</module>
				</modules>';
			}

		
			$data = array(
				'allowStartStopRecording' => 'false',
				'autoStartRecording' => 'true',
				'name' => ''.$subject,
				'record' => 'true',
				'welcome' => ''.$msg,
				'moderatorName' =>  ''.$teacher,
				'duration' => ''.$duration,
				'scheduleTime' => ''.time(),
				'schoolCode' => ''.$schoolCode,
				'serverURL' => ''.$serverURL,
				'presentation' => $modules
			);

            //print_r($data);exit;
			// use key 'http' even if you send the request to https://...
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);			
			if ( $result === false ) return;
			
		//	if(strpos($result, "success") == false) return;
			//echo $result;exit;
			
			  $arr = json_decode($result, true);
    			$dbV = array(
    				'meeting_id' => $arr['MeetingId'],
    				'routine_id' => $id,
    				'meeting_name' => $subject,
    				'moderatorJoinLink' => $arr['JoinMeetingURL'],
    				'attendeeJoinLink' => '',
    				'recordingLink' => '',
    				'duration' => $duration,
    				'start_time' => date('m/d/Y h:i', time()),
    				'participantCount' => 0,
    				'createDateTime' => date('m/d/Y h:i', time())
    			);
    
    			$this->db->insert('meetings', $dbV);
    			header("content-type: application-json");
    			echo json_encode(array(
    				'success' => true,
    				'join' => $arr['JoinMeetingURL']
    			));
			
			exit;
		}
	}
	//	SECTION ENDED

	//START SUBJECT section
	public function subject($param1 = '', $param2 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->subject_create();
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->crud_model->subject_update($param2);
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->subject_delete($param2);
			echo $response;
		}

		if($param1 == 'list'){
			$page_data['class_id'] = $param2;
			$this->load->view('backend/teacher/subject/list', $page_data);
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'subject';
			$page_data['page_title'] = 'subject';
			$this->load->view('backend/index', $page_data);
		}
	}

	public function class_wise_subject($class_id) {

		// PROVIDE A LIST OF SUBJECT ACCORDING TO CLASS ID
		$page_data['class_id'] = $class_id;
		$this->load->view('backend/teacher/subject/dropdown', $page_data);
	}
	//END SUBJECT section

	//START SYLLABUS section
	public function syllabus($param1 = '', $param2 = '', $param3 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->syllabus_create();
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->syllabus_delete($param2);
			echo $response;
		}

		if($param1 == 'list'){
			$page_data['class_id'] = $param2;
			$page_data['section_id'] = $param3;
			$this->load->view('backend/teacher/syllabus/list', $page_data);
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'syllabus';
			$page_data['page_title'] = 'syllabus';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END SYLLABUS section

	//START CLASS ROUTINE section
	public function routine($param1 = '', $param2 = '', $param3 = '', $param4 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->routine_create();
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->crud_model->routine_update($param2);
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->routine_delete($param2);
			echo $response;
		}

		if($param1 == 'filter'){
			$page_data['class_id'] = $param2;
			$page_data['section_id'] = $param3;
			$this->load->view('backend/teacher/routine/list', $page_data);
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'routine';
			$page_data['page_title'] = 'routine';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END CLASS ROUTINE section


	//START DAILY ATTENDANCE section
	public function attendance($param1 = '', $param2 = '', $param3 = ''){

		if($param1 == 'take_attendance'){
			$response = $this->crud_model->take_attendance();
			echo $response;
		}

		if($param1 == 'filter'){
			$date = '01 '.$this->input->post('month').' '.$this->input->post('year');
			$page_data['attendance_date'] = strtotime($date);
			$page_data['class_id'] = $this->input->post('class_id');
			$page_data['section_id'] = $this->input->post('section_id');
			$page_data['month'] = $this->input->post('month');
			$page_data['year'] = $this->input->post('year');
			$this->load->view('backend/teacher/attendance/list', $page_data);
		}

		if($param1 == 'student'){
			$page_data['attendance_date'] = strtotime($this->input->post('date'));
			$page_data['class_id'] = $this->input->post('class_id');
			$page_data['section_id'] = $this->input->post('section_id');
			$this->load->view('backend/teacher/attendance/student', $page_data);
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'attendance';
			$page_data['page_title'] = 'attendance';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END DAILY ATTENDANCE section


	//START EVENT CALENDAR section
	public function event_calendar($param1 = '', $param2 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->event_calendar_create();
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->crud_model->event_calendar_update($param2);
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->event_calendar_delete($param2);
			echo $response;
		}

		if($param1 == 'all_events'){
			echo $this->crud_model->all_events();
		}

		if ($param1 == 'list') {
			$this->load->view('backend/teacher/event_calendar/list');
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'event_calendar';
			$page_data['page_title'] = 'event_calendar';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END EVENT CALENDAR section


	//START EXAM section
	public function exam($param1 = '', $param2 = ''){

		if($param1 == 'create'){
			$response = $this->crud_model->exam_create();
			echo $response;
		}

		if($param1 == 'update'){
			$response = $this->crud_model->exam_update($param2);
			echo $response;
		}

		if($param1 == 'delete'){
			$response = $this->crud_model->exam_delete($param2);
			echo $response;
		}

		if ($param1 == 'list') {
			$this->load->view('backend/teacher/exam/list');
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'exam';
			$page_data['page_title'] = 'exam';
			$this->load->view('backend/index', $page_data);
		}
	}
	//END EXAM section

	//START MARKS section
	public function mark($param1 = '', $param2 = ''){

		if($param1 == 'list'){
			$page_data['class_id'] = $this->input->post('class_id');
			$page_data['section_id'] = $this->input->post('section_id');
			$page_data['subject_id'] = $this->input->post('subject');
			$page_data['exam_id'] = $this->input->post('exam');
			$this->crud_model->mark_insert($page_data['class_id'], $page_data['section_id'], $page_data['subject_id'], $page_data['exam_id']);
			$this->load->view('backend/teacher/mark/list', $page_data);
		}

		if($param1 == 'mark_update'){
			$this->crud_model->mark_update();
		}

		if(empty($param1)){
			$page_data['folder_name'] = 'mark';
			$page_data['page_title'] = 'marks';
			$this->load->view('backend/index', $page_data);
		}
	}

	// GET THE GRADE ACCORDING TO MARK
	public function get_grade($acquired_mark) {
		echo get_grade($acquired_mark);
	}
	//END MARKS sesction


	// BACKOFFICE SECTION

	//BOOK LIST MANAGER
	public function book($param1 = "", $param2 = "") {
		// adding book
		if ($param1 == 'create') {
			$response = $this->crud_model->create_book();
			echo $response;
		}

		// update book
		if ($param1 == 'update') {
			$response = $this->crud_model->update_book($param2);
			echo $response;
		}

		// deleting book
		if ($param1 == 'delete') {
			$response = $this->crud_model->delete_book($param2);
			echo $response;
		}
		// showing the list of book
		if ($param1 == 'list') {
			$this->load->view('backend/teacher/book/list');
		}

		// showing the index file
		if(empty($param1)){
			$page_data['folder_name'] = 'book';
			$page_data['page_title']  = 'books';
			$this->load->view('backend/index', $page_data);
		}
	}

	//MANAGE PROFILE STARTS
	public function profile($param1 = "", $param2 = "") {
		if ($param1 == 'update_profile') {
			$response = $this->user_model->update_profile();
			echo $response;
		}
		if ($param1 == 'update_password') {
			$response = $this->user_model->update_password();
			echo $response;
		}

		// showing the Smtp Settings file
		if(empty($param1)){
			$page_data['folder_name'] = 'profile';
			$page_data['page_title']  = 'manage_profile';
			$this->load->view('backend/index', $page_data);
		}
	}
	//MANAGE PROFILE ENDS

	public function time_table_wise () {
		$page_data['folder_name'] = 'my_records';
		$page_data['page_title']  = 'time_tablewise';
		$page_data['page_name']  = 'timetablewise';
		$this->load->view("/backend/index", $page_data);
	}

	public function classwise()
	{
		$page_data['folder_name'] = 'my_records';
		$page_data['page_title']  = 'classwise';
		$page_data['page_name']  = 'classwise';
		$this->load->view("/backend/index", $page_data);
	}

}
